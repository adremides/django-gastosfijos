# django-gastosfijos

Pequeña aplicación en django para llevar los gastos fijos

- context procesor para utilizar variables en todas las templates
Agregar en settings.py
```python
TEMPLATES = [
   {
       'BACKEND': 'django.template.backends.django.DjangoTemplates',
       'DIRS': [],
       'APP_DIRS': True,
       'OPTIONS': {
           'context_processors': [
               'django.template.context_processors.debug',
               'django.template.context_processors.request',
               'django.contrib.auth.context_processors.auth',
               'django.contrib.messages.context_processors.messages',

               'gastos.context_processor_file.datos_generales',  # agregar esta línea

           ],
       },
   },
]
```
