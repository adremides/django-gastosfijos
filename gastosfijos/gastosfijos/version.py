import subprocess


def get():
    # output = subprocess.getoutput("git log -1 --format=%at | xargs -I{} date -d @{} +%Y/%m/%d_%H:%M:%S")
    # output = subprocess.getoutput('git log -1 --date=format:"%Y/%m/%d %T" --format="%ad"')
    commit_hash = subprocess.getoutput('git log -1 --format="%H"')
    commit_date = subprocess.getoutput('git log -1 --date=format:"%Y.%m." --format="%ad"')

    return commit_date + commit_hash


def get_remote():
    commit_hash = subprocess.getoutput('git log -1 --format="%H" origin/master')
    commit_date = subprocess.getoutput('git log -1 --date=format:"%Y.%m." --format="%ad" origin/master')

    return commit_date + commit_hash
