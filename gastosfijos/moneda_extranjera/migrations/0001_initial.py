# Generated by Django 3.2.5 on 2022-07-01 22:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gastos', '0003_auto_20220324_2208'),
    ]

    operations = [
        migrations.CreateModel(
            name='Moneda',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('denominacion', models.CharField(max_length=100, unique=True, verbose_name='denominacion')),
            ],
            options={
                'verbose_name': 'Moneda',
                'verbose_name_plural': 'Monedas',
            },
        ),
        migrations.CreateModel(
            name='GastoCotizado',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('valor', models.DecimalField(decimal_places=2, max_digits=10)),
                ('gasto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gastos.gasto')),
                ('moneda', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='moneda_extranjera.moneda')),
            ],
        ),
        migrations.CreateModel(
            name='Cotizacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(auto_now_add=True)),
                ('valor', models.DecimalField(decimal_places=2, max_digits=10)),
                ('moneda', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='moneda_extranjera.moneda')),
            ],
            options={
                'verbose_name': 'Cotización',
                'verbose_name_plural': 'Cotizaciones',
            },
        ),
        migrations.AddConstraint(
            model_name='cotizacion',
            constraint=models.UniqueConstraint(fields=('fecha', 'moneda'), name='cotizacion_fecha_moneda'),
        ),
    ]
