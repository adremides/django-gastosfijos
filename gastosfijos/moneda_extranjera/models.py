from django.db import models
from django.utils import timezone


class Cotizacion(models.Model):
    fecha = models.DateField(auto_now_add=True)
    moneda = models.ForeignKey('Moneda', on_delete=models.CASCADE)
    valor = models.DecimalField(max_digits=10, decimal_places=2)

    objects = models.Manager()

    @classmethod
    def get_cotizacion(cls, denominacion):
        try:
            moneda = Moneda.objects.get(denominacion=denominacion)
            valor = cls.objects.get(fecha=timezone.localdate(), moneda=moneda)
        except cls.DoesNotExist:
            valor = None
        return valor

    def __str__(self):
        return str(self.valor)

    class Meta:
        verbose_name = 'Cotización'
        verbose_name_plural = 'Cotizaciones'
        constraints = [
            models.UniqueConstraint(fields=['fecha', 'moneda'], name='cotizacion_fecha_moneda')
        ]


class Moneda(models.Model):
    denominacion = models.CharField('denominacion', max_length=100, unique=True)

    objects = models.Manager()

    def __str__(self):
        return self.denominacion

    class Meta:
        verbose_name = 'Moneda'
        verbose_name_plural = 'Monedas'


class GastoCotizado(models.Model):
    gasto = models.ForeignKey('gastos.Gasto', on_delete=models.CASCADE)
    moneda = models.ForeignKey('Moneda', on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now_add=True)
    valor = models.DecimalField(max_digits=10, decimal_places=2)

    objects = models.Manager()

    def __str__(self):
        return f'Valor de {self.moneda} fue {self.valor} para {self.gasto}'
