from django.conf import settings
from gastos.models import Configuracion
from .models import Cotizacion, Moneda
from bs4 import BeautifulSoup
import requests


def cotizar(denominacion='Dólar'):
    configuracion = Configuracion.obtener_configuracion()

    cotizado = {
        'valor': 0,
        'valido': False,
        'observacion': '',
    }

    try:
        if configuracion.cotizar:
            moneda = Moneda.objects.get(denominacion=denominacion)
            valor = Cotizacion.get_cotizacion(denominacion)
            cotizado['valor'] = valor
            if configuracion.dolar_api == 'api-dolar':
                if not valor:
                    print('va a buscar el valor a la API')
                    data = requests.get('https://api-dolar-argentina.herokuapp.com/api/nacion')
                    cotizado['valor'] = data.json()["venta"]
                    Cotizacion.objects.create(moneda=moneda, valor=data.json()["venta"])
                cotizado['valido'] = True
            if configuracion.dolar_api == 'dolarsi':
                if not valor:
                    print('va a buscar el valor a la API')
                    xml_data = requests.get('https://www.dolarsi.com/api/dolarSiInfo.xml')
                    bs_data = BeautifulSoup(xml_data.text, "xml")
                    casa6 = bs_data.find('casa6')
                    venta = casa6.find('venta')
                    cotizado['valor'] = venta.text
                    Cotizacion.objects.create(moneda=moneda, valor=venta.text)
                cotizado['valido'] = True
            if configuracion.dolar_api == '':
                cotizado['observacion'] = 'API no configurada'
    except Exception as e:
        cotizado['observacion'] = e
    return cotizado
