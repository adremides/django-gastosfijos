from django.contrib import admin
from .models import Cotizacion, Moneda, GastoCotizado


@admin.register(Moneda)
class MonedaAdmin(admin.ModelAdmin):
    model = Moneda
    ordering = ['denominacion']
    search_fields = ['denominacion']


@admin.register(Cotizacion)
class CotizacionAdmin(admin.ModelAdmin):
    model = Cotizacion
    ordering = ['fecha']
    search_fields = ['moneda']
    list_display = [
        'fecha',
        'moneda',
        'valor'
    ]


@admin.register(GastoCotizado)
class GastoCotizadoAdmin(admin.ModelAdmin):
    model = GastoCotizado
    ordering = ['fecha', 'gasto']
    search_fields = ['gasto']
