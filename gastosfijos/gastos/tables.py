import django_tables2 as tables
from .models import Gasto  # , Movimiento
# from django_tables2.utils import Accessor
# import itertools


class SummingColumn(tables.Column):
    def render_footer(self, bound_column, table):
        print(self)
        return sum(bound_column.accessor.resolve(row) for row in table.data)


def obtener_tipo(**kwargs):
    # dato = kwargs.get('tipo', None)
    # print(kwargs)
    # print(kwargs['record']['tipo'])
    return kwargs['record']['tipo']


def obtener_style(**kwargs):
    # dato = kwargs.get('tipo', None)
    # print(kwargs)
    # print("tipo:", kwargs['record']['tipo'])
    style = f"background-color: {kwargs['record']['bg_color']}"
    print("style: ", style)
    return style


class GastoTable(tables.Table):
    gasto = tables.LinkColumn(
        'admin:gastos_movimiento_change',
        # text=PRUEBA,
        args=[tables.A('pk_movimiento')],  # Esto viene de views.py->lista_gastos_por_fecha()->campos
        footer='Total:'
    )

    # # mes = tables.Column(verbose_name='Mes')
    # prueba = ({'verbose_name': 'datos'})
    # print(movimiento)
    # importe = SummingColumn(**prueba)

    class Meta:
        model = Gasto
        fields = (
            # 'titular',
            'gasto',
            # 'mes',
            # 'importe',
        )
        # fields = ('totos',
        #           'mes_importe',
        #          )
        # template_name = "gastos/tabla.html"
        row_attrs = {
            # lambda table: table.data #'test' #lambda record: record.gasto
            'tipo':  obtener_tipo,  # lambda record: 'ok' if record['gasto'] is None else 'warning'
            # sobrescribir el CSS con el color personalizado del gasto (diferentes entidades_fnancieras)
            # 'style': 'background-color: #000000'
            'style': obtener_style,
        }

    # def __init__(self, *args, **kwargs):
    #  super(GastoTable, self).__init__(*args, **kwargs)
    #  self.counter = itertools.count()
    #
    # def render_row_number(self):
    #  return '%d' % next(self.counter)
    #
    # def render_id(self, value):
    #  return '%s' % value


class TablaGasto(tables.Table):
    # def define_table(columns):
    #     attrs = dict((c, tables.Column()) for c in columns)
    #     klass = type('DynamicTable', (tables.Table,), attrs)
    #     return klass
    importe = tables.Column()

    class Meta:
        model = Gasto
        orderable = False


class Tablas(tables.Table):
    gasto = tables.Column(footer='Total:')

    class Meta:
        model = Gasto
        fields = (
            # 'titular',
            'gasto',
            # 'mes',
            # 'importe',
        )


class TablasDict(tables.Table):
    pass
