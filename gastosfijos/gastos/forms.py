from django import forms

# from django.utils import timezone
# from datetime import datetime
# from dateutil.relativedelta import relativedelta

from .models import Gasto, Permanente
from month.widgets import MonthSelectorWidget


class CompararGastosForm(forms.ModelForm):

    class Meta:
        model = Gasto
        fields = (  # 'movimiento',
                  'cupon',
                  'mes',
                    # 'importe',
                  'observacion',
                  )
        exclude = ['movimiento']

    def mov_concepto(self, obj):
        return obj.__str__()
    #     return obj.movimiento.concepto.descripcion
    mov_concepto.short_description = 'Concepto'
    mov_concepto.admin_order_field = 'movimiento__concepto__descripcion'


class PermanenteAccionForm(forms.Form):

    mes_fin = forms.CharField(
        widget=MonthSelectorWidget,
        required=True,
        help_text='Mes de finalización',
        # initial='2019-07-01',
    )

    # Pensé que era posible hacer el suggest de mes_fin en el init, pero genera problemas con el request.
    # Se hace en admin.py cuando se pasa el form al context.
    # def __init__(self, permanente=None, *args, **kwargs):
    #     super(PermanenteAccionForm, self).__init__(*args, **kwargs)
    #     if permanente:
    #         mes_anterior_al_actual = timezone.localdate() - relativedelta(months=1)
    #         if mes_anterior_al_actual < datetime.strptime(str(permanente.mes_inicio), '%Y-%m').date():
    #             mes_fin_sugerido = permanente.mes_inicio
    #         else:
    #             mes_fin_sugerido = mes_anterior_al_actual
    #         self.fields['mes_fin'].initial = mes_fin_sugerido

    # 01/07/2019 - Se debería poder quitar "user" ya que no se utiliza, pero deja de funcionar, no sé por qué aún

    def form_action(self, permanente, user):
        raise NotImplementedError()

    def save(self, permanente, user):
        try:
            permanente, accion = self.form_action(permanente, user)
        except Exception as e:
            error_message = str(e)
            self.add_error(None, error_message)
            raise

        return permanente, accion


class FinalizarForm(PermanenteAccionForm):

    def form_action(self, permanente, user):
        return Permanente.finalizar(
            id_permanente=permanente.pk,
            mes_fin=self.cleaned_data['mes_fin'],
        )


class ReactivarForm(PermanenteAccionForm):
    importe_reactivacion = forms.FloatField(
        required=True,
        help_text='Importe al reactivar',
    )

    def form_action(self, permanente, user):
        return Permanente.reactivar(
            id_permanente=permanente.pk,
            mes_fin=self.cleaned_data['mes_fin'],
            importe=self.cleaned_data['importe_reactivacion'],
        )
