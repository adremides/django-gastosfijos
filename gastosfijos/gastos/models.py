from django.db import models, transaction
# from django.db.models import PositiveIntegerField

# from django.db.models import F
from django.utils import timezone

from month.models import MonthField
from django.core.validators import MinValueValidator
# from django.core.validators import MaxValueValidator

# from django.utils import timezone
from datetime import datetime
from dateutil.relativedelta import relativedelta

from colorfield.fields import ColorField


class PositiveIntegerRangeField(models.PositiveIntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.PositiveIntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(PositiveIntegerRangeField, self).formfield(**defaults)


class Concepto(models.Model):
    descripcion = models.CharField('Descripción', max_length=100, unique=True)
    detalle = models.CharField('Detalle', max_length=100, default="cambiar")

    objects = models.Manager()

    def __str__(self):
        return self.descripcion


class Titular(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    entidades_financieras = models.ManyToManyField(
        'gastos.Entidad_financiera',
        verbose_name='entidades_financieras',
        blank=True,
        help_text='Entidades financieras a las que está adherido el usuario.',
        # related_name="entidades_financieras",
        # related_query_name="user",
    )

    objects = models.Manager()

    def __str__(self):
        return self.nombre

    def has_entidad_finaciera(self, entidad_financiera):
        return True if entidad_financiera in self.entidades_financieras.all() else False

    def get_gastos(self):
        qs = Gasto.objects.all().filter(movimiento__titular__nombre=self.nombre).exclude(movimiento__plan_zeta=True)
        return qs

    def get_gastos_y_permanentes_por_entidad_financiera(self):
        lista = []
        for entidad_financiera in Entidad_financiera.objects.all():
            if self.has_entidad_finaciera(entidad_financiera):
                lista.append({
                    "gastos": self.get_gastos().filter(movimiento__entidad_financiera=entidad_financiera),
                    "permanentes": Permanente.objects.filter(entidad_financiera=entidad_financiera)
                })

        return lista

    class Meta:
        verbose_name_plural = 'Titulares'


class ColorElemento(models.Model):
    descripcion = models.CharField(max_length=50)
    gasto = ColorField(format='hexa')
    permanente = ColorField(format='hexa')

    def __str__(self):
        return self.descripcion


class Entidad_financiera(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    # Agregar un (o varios) campo que permita elegir un color.
    # Utilizar diferentes colores que combinen para diferenciar los Gastos de los Permanentes
    color = models.ForeignKey('gastos.ColorElemento', blank=True, null=True, on_delete=models.PROTECT)

    objects = models.Manager()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = 'Entidades financieras'


class Permanente(models.Model):
    entidad_financiera = models.ForeignKey('gastos.Entidad_financiera', on_delete=models.PROTECT)
    concepto = models.ForeignKey('gastos.Concepto', on_delete=models.PROTECT)
    cupon = models.CharField(max_length=50, unique=False, default='0')
    mes_inicio = MonthField()
    mes_fin = MonthField(help_text='Vacío cuenta para todos los meses a partir del inicio', blank=True, null=True)
    importe = models.DecimalField(max_digits=11, decimal_places=2)

    objects = models.Manager()

    # Controlar que mes_inicio y mes_fin para un determinado concepto no se superpongan con otros Permanentes
    # Empezar a averiguar por aca:
    # https://stackoverflow.com/questions/35096607/
    # how-to-enforce-different-values-in-multiple-foreignkey-fields-for-django

    def __str__(self):
        return "%s de %s a %s" % (self.concepto,
                                  str(self.mes_inicio),
                                  str(self.mes_fin) if self.mes_fin else "hoy")

    @classmethod
    def finalizar(cls, id_permanente, mes_fin):

        with transaction.atomic():
            permanente = cls.objects.select_for_update().get(id=id_permanente)

            # if cls.mes_inicio < mes_fin - 1:
            #     # TODO: raise errors.InvalidAmount(amount)
            #     pass

            permanente.mes_fin = mes_fin

            permanente.save(update_fields=[
                'mes_fin',
            ])

            accion = None

        return permanente, accion

    @classmethod
    def reactivar(cls, id_permanente, mes_fin, importe):
        nuevo_mes_inicio = datetime.strptime(mes_fin, '%Y-%m-%d').date() + relativedelta(months=1)

        with transaction.atomic():
            permanente = cls.objects.select_for_update().get(id=id_permanente)
            permanente_nuevo = Permanente.objects.create(
                entidad_financiera=permanente.entidad_financiera,
                concepto=permanente.concepto,
                cupon=permanente.cupon,
                mes_inicio=nuevo_mes_inicio,
                mes_fin=None,
                importe=importe
            )

            # if cls.mes_inicio < mes_fin - 1:
            #     # TODO: raise errors.InvalidAmount(amount)
            #     pass

            permanente.mes_fin = mes_fin

            permanente.save(update_fields=[
                'mes_fin',
            ])

            permanente_nuevo.save()

            accion = None

        return permanente, accion


class Movimiento(models.Model):
    titular = models.ForeignKey('gastos.Titular', on_delete=models.PROTECT)
    entidad_financiera = models.ForeignKey('gastos.Entidad_financiera', on_delete=models.PROTECT)
    concepto = models.ForeignKey('gastos.Concepto', on_delete=models.PROTECT)
    cupon = models.CharField(max_length=50, unique=False, default='0')
    mes_inicio = MonthField()
    cantidad_cuotas = models.PositiveIntegerField(validators=[MinValueValidator(1)], default=1)
    importe = models.DecimalField(help_text=str(cantidad_cuotas), max_digits=11, decimal_places=2)
    observacion = models.CharField('Observación', max_length=1000, blank=True, null=True)
    plan_zeta = models.BooleanField(default=False, blank=True)
    plan_zeta_cargado = models.BooleanField(default=False, blank=True)
    valor_intercambio = models.PositiveIntegerField(validators=[MinValueValidator(1)], default=1)

    objects = models.Manager()

    def save(self, *args, **kwargs):

        estoy_creando = not self.pk

        super(Movimiento, self).save(*args, **kwargs)

        if estoy_creando:  # si el registro aún no fue creado estoy creándolo...
            print("crando gastos...")
            meses = []
            for mes in range(0, self.cantidad_cuotas):
                meses.append(self.mes_inicio + mes)
            for mes in meses:
                registro_gasto = Gasto.objects.create(
                    movimiento=self,
                    cupon=self.cupon,
                    mes=mes,
                    importe=self.importe,
                    observacion=self.observacion,
                    procesado=False,
                    valor_intercambio=self.valor_intercambio,
                )
                registro_gasto.save()
        else:  # ... lo estoy editando
            registros_gastos = Gasto.objects.filter(movimiento=self.pk).update(
                cupon=self.cupon,
                observacion=self.observacion,
            )
            print(registros_gastos)
            # registros_gastos.save()

    # def delete_selected(self, request, obj):
    #     super(Movimiento, self).delete(*args, **kwargs)
    #     for o in obj.all():
    #         qs = Gasto.objects.filter(movimiento=o)
    #         # o.delete()
    #         print(qs)

    def __str__(self):
        return "%s de %s, %s" % (self.entidad_financiera,
                                 self.titular,
                                 self.concepto)

# class GastoQuerySet(models.QuerySet):
#     def delete(self, *args, **kwargs):
#         for obj in self:
#             obj.gasto.delete()
#             print(obj.gasto)
#         super(GastoQuerySet, self).delete(*args, **kwargs)


class Gasto(models.Model):
    movimiento = models.ForeignKey('gastos.Movimiento', on_delete=models.CASCADE)
    cupon = models.CharField(max_length=50, unique=False, default='0')
    mes = MonthField()
    importe = models.DecimalField(max_digits=11, decimal_places=2)
    observacion = models.CharField('Observación', max_length=1000, blank=True, null=True)
    procesado = models.BooleanField(default=False)
    valor_intercambio = models.PositiveIntegerField(validators=[MinValueValidator(1)], default=1)

    objects = models.Manager()

    def __str__(self):
        return "%s (cuota %s de %s)" % (
            self.movimiento.concepto.descripcion,
            self.mes - self.movimiento.mes_inicio + 1,
            self.movimiento.cantidad_cuotas
        )

    def delete(self, *args, **kwargs):
        self.movimiento.delete()
        super(Gasto, self).delete(*args, **kwargs)
        print("delete gasto")


class Cierre(models.Model):
    entidad_financiera = models.OneToOneField('gastos.Entidad_financiera', on_delete=models.PROTECT)
    dia_de_cierre = PositiveIntegerRangeField('Día de cierre', min_value=1, max_value=31, default=24)

    objects = models.Manager()

    @classmethod
    def obtener_mes_actual(cls, entidad_financiera):
        nombre_entidad_financiera = entidad_financiera  # 'Tarjeta Naranja'
        cierre_naranja = cls.objects.filter(entidad_financiera__nombre=nombre_entidad_financiera).order_by('-id')
        mes_actual = timezone.localdate()
        if cierre_naranja:
            # Si el día actual es mayor al día guardado de cierre...
            if timezone.localdate().day > cierre_naranja[0].dia_de_cierre:
                # ... el mes actual es el mes próximo
                # Por ejemplo si la fecha de cierre es el 23 y el día actual es 24/03
                # entonces el mes actual en lugar de ser marzo (03) será abril (04).
                mes_actual = timezone.localdate() + relativedelta(months=1)
        return mes_actual

    def __str__(self):
        return str(self.entidad_financiera) + " - " + str(self.dia_de_cierre)


class ResumenTNTemp(models.Model):
    mes = MonthField()
    tipo = models.CharField(max_length=10)
    fecha = models.DateField(null=True)
    tarjeta = models.CharField(max_length=7, null=True)
    cupon = models.CharField(max_length=15, unique=True)
    detalle = models.CharField(max_length=100)
    cuota = models.CharField(max_length=10, null=True)
    importe = models.DecimalField(max_digits=10, decimal_places=2)
    moneda = models.CharField(max_length=1)
    considerar = models.BooleanField()
    procesado = models.BooleanField()

    # @classmethod
    def excluir(self):
        self.considerar = False
        self.save()

    objects = models.Manager()


class NombreParcial(models.Model):
    detalle = models.CharField(max_length=100, null=False, blank=False)

    objects = models.Manager()

    def __str__(self):
        return self.detalle

    class Meta:
        verbose_name_plural = 'Nombres Paciales'


class GastoPermanente(models.Model):
    CAMPO_PDF_CHOICES = [
        ('fecha', 'Fecha'),
        ('tarjeta', 'Tarjeta'),
        ('cupon', 'Cupón'),
        ('detalle', 'Detalle'),
        ('cuota_plan', 'Cuota/Plan'),
        ('importe', 'Importe'),
        ('dolares', 'Dólares'),
    ]
    campo_pdf = models.CharField(max_length=200, choices=CAMPO_PDF_CHOICES)
    detalle = models.CharField(max_length=100, null=False, blank=False)

    objects = models.Manager()

    def __str__(self):
        return self.detalle

    class Meta:
        verbose_name_plural = 'Gastos Permanentes'


class Configuracion(models.Model):
    CAMPO_DOLAR_API_CHOICES = [
        ('api-dolar', 'api-dolar-argentina.herokuapp.com'),
        ('dolarsi', 'www.dolarsi.com/api/dolarSiInfo.xml'),
    ]
    detalle_meses_antes = models.PositiveIntegerField(validators=[MinValueValidator(1)], default=1)
    detalle_meses_despues = models.PositiveIntegerField(validators=[MinValueValidator(1)], default=10)
    dolar_api = models.CharField(max_length=200, choices=CAMPO_DOLAR_API_CHOICES, blank=True, null=False)
    cotizar = models.BooleanField(default=False)

    objects = models.Manager()

    # def __str__(self):
    #     return self.detalle

    @classmethod
    def obtener_configuracion(cls):
        registros = cls.objects.all()
        return registros[0]

    class Meta:
        verbose_name_plural = 'Configuraciones'


class GastoExcluidoResumen(models.Model):
    entidad_financiera = models.ForeignKey('gastos.Entidad_financiera', on_delete=models.PROTECT)
    cupon = models.CharField(max_length=50, unique=False, default='0')
    titular = models.ForeignKey('gastos.Titular', on_delete=models.PROTECT)

    objects = models.Manager()

    class Meta:
        verbose_name_plural = 'Gastos Excluidos del Resumen'

