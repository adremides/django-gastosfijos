from .models import Gasto, Permanente, ResumenTNTemp, Cierre
from django.db.models import Q, Sum, Count, DecimalField
from django.db.models.functions import Coalesce
# from django.utils import timezone
# from dateutil.relativedelta import relativedelta
import decimal


class ComparacionTarjetaNaranja(object):

    @classmethod
    def detalle_bd_nombre(cls, record):
        return record['detalle(bd)']

    comparativas = {
        'total_gastos': {'titulo': '',
                         'tipo': '2col_bd_y_f',
                         'datos': {'bd': 0, 'factura': 0}},

        'total_importes': {'titulo': '',
                           'tipo': 'total_importes_diferencia',
                           'datos': {'bd': 0, 'factura': 0, 'diferencia': 0}},

        'gastos_sin_cupon': {'titulo': 'Gastos sin cupón',
                             'tipo': '6col_bd_y_f'},

        'gastos_posibles_sin_cupon': {'titulo': 'Gastos que posiblemente no tengan cupón',
                                      'tipo': '6col_bd_y_f'},

        'gastos_falta_cargar': {'titulo': 'Gastos que faltan en la Base de Datos',
                                'tipo': '4col_f'},

        'gastos_detalle_diferente': {'titulo': 'Gastos que difieren en el Detalle',
                                     'tipo': '6col_bd_y_f'},

        'gastos_mismo_importe': {'titulo': 'Gastos que coinciden en el importe',
                                 'tipo': '6col_bd_y_f'},

        'gastos_importe_diferente': {'titulo': 'Gastos con importe diferente',
                                     'tipo': '6col_bd_y_f'},

        'gastos_importe_diferente_bd': {'titulo': 'Gastos cargados en la BD con importe diferente',
                                        'tipo': '6col_bd_y_f_sin_relacion'},

        'gastos_plan_zeta_faltan_cargar': {'titulo': 'Gastos Plan Zeta en factura que faltan cargar',
                                           'tipo': '4col_f'},

        'permanentes_posibles_sin_cupon': {'titulo': 'Permanentes que posiblemente no tengan cupón',
                                           'tipo': '6col_bd_y_f'},

        'permanentes_mismo_importe': {'titulo': 'Permanentes que coinciden en importe',
                                      'tipo': '6col_bd_y_f'},

        'permanentes_falta_cargar': {'titulo': 'Permanentes que faltan en la Base de Datos',
                                     'tipo': '4col_f'},

        'permanentes_mismo_detalle': {'titulo': 'Permanentes que coinciden en detalle',
                                      'tipo': '6col_bd_y_f'}
    }

    def __init__(self):
        for item in self.comparativas:
            self.comparativas[item]['datos'] = []
        self.comparativas['total_gastos']['datos'] = {'bd': 0, 'factura': 0}
        self.comparativas['total_importes']['datos'] = {'bd': 0, 'factura': 0}

    def comparar(self):
        nombre_entidad_financiera = 'Tarjeta Naranja'
        mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera).strftime('%Y-%m')

        bd_gastos = Gasto.objects.filter(mes=mes_actual)
        bd_gastos_sin_procesar = Gasto.objects.filter(mes=mes_actual).exclude(procesado=True)
        factura_gastos = ResumenTNTemp.objects.filter(tipo='gasto', considerar=True)
        factura_gastos_sin_procesar = ResumenTNTemp.objects.filter(tipo='gasto', considerar=True).exclude(procesado=True)
        # No traer los gastos en Plan Zeta, estos se procesan a parte
        factura_gastos_sin_procesar = factura_gastos_sin_procesar.filter(~Q(cuota='ta'))

        total_zeta = ResumenTNTemp.objects.filter(cuota='ta').aggregate(
            importe=Sum('importe'), cantidad=Count('detalle')
        )

        # Reseteo el campo 'procesado'
        bd_gastos.update(procesado=False)
        factura_gastos.update(procesado=False)

        for gasto_bd in bd_gastos:
            importe_factura = factura_gastos.filter(importe=gasto_bd.importe, procesado=False)

            if len(importe_factura) > 0:
                gasto_bd.procesado = True
                gasto_bd.save()
                importe_factura[0].procesado = True
                importe_factura[0].save()
        if len(bd_gastos_sin_procesar):
            self.comparativas['gastos_importe_diferente_bd']['datos'].append({
                "factura": list(factura_gastos_sin_procesar),
                "bd": list(bd_gastos_sin_procesar)
            })

        # Reseteo el campo 'procesado'
        bd_gastos.update(procesado=False)
        factura_gastos.update(procesado=False)

        # print('======== BASE DE DATOS =========')
        # print('---- GASTOS SIN PROCESAR ----')
        # for gasto_sin_procesar in bd_gastos_sin_procesar:
        #     print(
        #         'gasto: %s \nimporte: %s' % (
        #             gasto_sin_procesar.movimiento.concepto.descripcion, gasto_sin_procesar.importe
        #         )
        #     )
        # print('---- GASTOS PROCESADOS ----')
        # for gasto_procesado in bd_gastos.filter(procesado=True):
        #     print(
        #         'gasto: %s \nimporte: %s' % (
        #             gasto_procesado.movimiento.concepto.descripcion, gasto_procesado.importe
        #         )
        #     )
        # print('---- gastos no encontrado importe ----')
        # print(no_encontrado_importe)
        # print('-----------')
        # print('======== FACTURA =========')
        # print('---- GASTOS SIN PROCESAR ----')
        # for gasto_sin_procesar in factura_gastos_sin_procesar:
        #     print(
        #         'gasto: %s \nimporte: %s' % (
        #             gasto_sin_procesar.detalle, gasto_sin_procesar.importe
        #         )
        #     )

        bd_gastos_cupon_cero = bd_gastos.filter(movimiento__cupon='0')

        # Reseteo el campo 'procesado'
        bd_gastos_cupon_cero.update(procesado=False)
        factura_gastos.update(procesado=False)

        # Para cada gasto con cupón cero en la BD, relacionar con un registro de la factura
        # Este proceso guarda lo que se va a mostrar en el cuadro comparativo gastos_sin_cupon
        for gasto_bd in bd_gastos_cupon_cero:
            gasto_factura = factura_gastos.filter(
                Q(importe=gasto_bd.importe) & Q(detalle=gasto_bd.movimiento.concepto.descripcion)
            )
            bd_detalle = bd_gastos_cupon_cero.filter(pk=gasto_bd.pk)

            if len(gasto_factura) == 1:
                gasto_bd.procesado = True
                gasto_bd.save()
                gasto_factura[0].procesado = True
                gasto_factura[0].save()
                self.comparativas['gastos_sin_cupon']['datos'].append({"factura": gasto_factura[0], "bd": bd_detalle})

        # print('------- bd_gastos -------')
        # for g in bd_gastos:
        #     print('pk: %s - gasto: %s - procesado: %s' % (g.pk, g.movimiento.concepto.descripcion, g.procesado))
        # print('------- factura_gastos -------')
        # for gf in factura_gastos:
        #     print('pk: %s - gasto: %s - procesado: %s' % (gf.pk, gf.detalle, gf.procesado))

        print("COMPROBANDO GASTOS EN PLAN ZETA")
        # Recorro los gastos que son del plan zeta en la factura y aún no fueron cargados como gastos
        bd_gastos_plan_zeta = Gasto.objects.filter(mes=mes_actual).filter(movimiento__plan_zeta=True)
        bd_gastos_plan_zeta = bd_gastos_plan_zeta.filter(movimiento__plan_zeta_cargado=False)
        factura_gastos_plan_zeta = ResumenTNTemp.objects.filter(tipo='gasto').filter(cuota='ta')
        for factura_gasto in factura_gastos_plan_zeta:
            print("Gasto en factura: %s, importe: %s" % (factura_gasto.detalle, factura_gasto.importe))
            bd_importe = bd_gastos_plan_zeta.filter(importe=factura_gasto.importe)

            print("Importes encontrados en la BD: %s" % len(bd_importe))

            if not len(bd_importe):
                self.comparativas['gastos_plan_zeta_faltan_cargar']['datos'].append(factura_gasto)

        print('--====== ENCONTRADOS =======--')
        for gasto_factura in factura_gastos_sin_procesar:
            bd_cupon = bd_gastos_sin_procesar.filter(cupon=gasto_factura.cupon)
            bd_detalle = bd_gastos_sin_procesar.filter(movimiento__concepto__descripcion=gasto_factura.detalle)
            bd_importe = bd_gastos_sin_procesar.filter(importe=gasto_factura.importe)
            existe = bool(len(bd_cupon) and len(bd_detalle) and len(bd_importe))

            print('FACTURA cupon: %s - detalle: %s - importe: %s' % (
                gasto_factura.cupon, gasto_factura.detalle, gasto_factura.importe
            ))
            print('BD cupon: %s - detalle: %s - importe: %s' % (
                len(bd_cupon), len(bd_detalle), len(bd_importe)
            ))
            if existe:
                pass
            else:
                # Si encontró 1 detalle y 1 importe pero no un cupón
                # if len(bd_detalle) == 1 and len(bd_importe) == 1 and len(bd_cupon) < 1:
                #     self.comparativas['gastos_sin_cupon']['datos'].append(gasto_factura)
                # Si encontró 1 o más detalles con cupon 0 pero no un cupón
                if len(bd_detalle.filter(cupon='0')) > 0 and len(bd_cupon) < 1:
                    # si hay más gastos iguales en la factura que en la bd...
                    # si hay más gastos iguales en la bd que en la factura...
                    self.comparativas['gastos_posibles_sin_cupon']['datos'].append({"factura": gasto_factura,
                                                                                    "bd": bd_detalle})
                # Si encontró un cupón pero no el detalle
                elif len(bd_detalle) < 1 and len(bd_cupon) > 0:
                    self.comparativas['gastos_detalle_diferente']['datos'].append({"factura": gasto_factura,
                                                                                   "bd": bd_cupon})
                # Si encontró el cupón pero no coincide el importe
                elif len(bd_importe) < 1 and len(bd_cupon) > 0:
                    if len(bd_cupon) > 0:
                        self.comparativas['gastos_importe_diferente']['datos'].append({"factura": gasto_factura,
                                                                                       "bd": bd_cupon})
                    elif len(bd_detalle) > 0:
                        self.comparativas['gastos_importe_diferente']['datos'].append({"factura": gasto_factura,
                                                                                       "bd": bd_detalle})
                # No coincide ni el detalle, ni el cupón pero hay uno con igual importe
                elif len(bd_importe) > 0 and len(bd_cupon) < 1 and len(bd_detalle) < 1:
                    self.comparativas['gastos_mismo_importe']['datos'].append({"factura": gasto_factura,
                                                                               "bd": bd_importe})
                else:
                    self.comparativas['gastos_falta_cargar']['datos'].append(gasto_factura)

        # self.total_gastos["bd"] = len(bd_gastos)
        # self.total_gastos["factura"] = len(factura_gastos)
        self.comparativas['total_gastos']['datos']["bd"] = len(bd_gastos.filter(~Q(movimiento__plan_zeta=True)))
        self.comparativas['total_gastos']['datos']["factura"] = len(factura_gastos)
        # Le resto la cantidad de gastos en Plan Zeta. Aún no se consideran un gasto.
        # Luego de elegir la cantidad de cuotas ya se lo considera un gasto.
        self.comparativas['total_gastos']['datos']["factura"] -= total_zeta['cantidad']

        bd_permanentes_mes = Permanente.objects.filter(
            (Q(mes_fin=0) & Q(mes_inicio__lte=mes_actual)) |
            (Q(mes_inicio__lte=mes_actual) & Q(mes_fin__gte=mes_actual))
        )

        factura_permanentes = ResumenTNTemp.objects.filter(tipo='permanente')

        for p in factura_permanentes:
            bd_cupon = bd_permanentes_mes.filter(cupon=p.cupon)
            bd_detalle = bd_permanentes_mes.filter(concepto__descripcion__icontains=p.detalle)
            bd_importe = bd_permanentes_mes.filter(importe=p.importe)
            # existe = bool(len(cupon) and len(detalle) and len(importe))
            existe = bool(len(bd_detalle) and len(bd_importe))

            if not existe:
                # Si encontró 1 detalle y 1 importe pero no un cupón
                if len(bd_detalle) == 1 and len(bd_importe) == 1 and len(bd_cupon) < 1:
                    self.comparativas['permanentes_posibles_sin_cupon']['datos'].append({'factura': p,
                                                                                         'bd': bd_detalle})
                # No coincide el detalle pero hay uno con igual importe
                elif len(bd_importe) > 0 and len(bd_detalle) < 1:
                    self.comparativas['permanentes_mismo_importe']['datos'].append({'factura': p, 'bd': bd_importe})
                # No coincide el importe pero hay 1 o más con igual detalle
                elif len(bd_importe) < 1 and len(bd_detalle) > 0:
                    self.comparativas['permanentes_mismo_detalle']['datos'].append({'factura': p, 'bd': bd_detalle})
                else:
                    self.comparativas['permanentes_falta_cargar']['datos'].append(p)

        total_bd_gastos = bd_gastos.filter(~Q(movimiento__plan_zeta=True))\
            .aggregate(total=Coalesce(Sum('importe'), 0, output_field=DecimalField()))
        total_bd_permanentes = bd_permanentes_mes.aggregate(total=Sum('importe'))
        total_bd = total_bd_gastos['total'] + total_bd_permanentes['total']

        total_factura = ResumenTNTemp.objects.aggregate(total=Sum('importe'))

        print('total_factura: %s' % total_factura)

        self.comparativas['total_importes']['datos']['bd'] = total_bd.quantize(decimal.Decimal('.01'),
                                                                               rounding=decimal.ROUND_DOWN)

        self.comparativas['total_importes']['datos']['factura'] = total_factura['total'].quantize(
            decimal.Decimal('.01'), rounding=decimal.ROUND_DOWN
        )
        self.comparativas['total_importes']['datos']['diferencia'] = (
            self.comparativas['total_importes']['datos']['factura'] - self.comparativas['total_importes']['datos']['bd']
        ).quantize(decimal.Decimal('.01'), rounding=decimal.ROUND_DOWN)
