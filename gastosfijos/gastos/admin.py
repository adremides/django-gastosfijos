# from admin_totals.admin import ModelAdminTotals
from django.contrib import admin, messages
from django.utils.html import format_html
from django.conf.urls import url
from django.urls import reverse
from django.db.models import Q, Sum
# from django.db.models.functions import Coalesce
from .models import *
from .forms import FinalizarForm, ReactivarForm
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.utils import timezone
from moneda_extranjera import functions
from decimal import Decimal
import moneda_extranjera
import distutils.util


@admin.register(Concepto)
class ConceptoAdmin(admin.ModelAdmin):
    model = Concepto
    ordering = ['descripcion']
    search_fields = ['descripcion']


@admin.register(Movimiento)
class MovimientoAdmin(admin.ModelAdmin):
    model = Movimiento
    list_display = (
        'titular',
        'entidad_financiera',
        'concepto',
        'cupon',
        'mes_inicio',
        'cantidad_cuotas',
        'importe',
        'plan_zeta',
        'observacion',

    )
    fields = (
        'titular',
        'entidad_financiera',
        'concepto',
        'cupon',
        'mes_inicio',
        'cantidad_cuotas',
        'importe',
        'plan_zeta',
        'observacion',
    )
    search_fields = ('concepto__descripcion',)
    list_filter = (
        'titular',
    )
    # autocomplete_fields = ('concepto',)

    # Modificar importe inicial que viene en los parametros GET
    # con el importe calculado si es moneda extranjera
    def get_changeform_initial_data(self, request):
        """
        Get the initial form data from the request's GET params.
        """
        initial = super(MovimientoAdmin, self).get_changeform_initial_data(request)
        try:
            if distutils.util.strtobool(initial['es_moneda_extranjera']):
                cotizacion_dolar = moneda_extranjera.functions.cotizar()
                valor_en_moneda_extranjera = initial["importe"]
                initial['importe'] = Decimal(initial['importe']) * Decimal(cotizacion_dolar["valor"])
                initial['observacion'] += f' {valor_en_moneda_extranjera} dólares a {cotizacion_dolar["valor"]}'
        except Exception as e:
            type(e)
        return initial

    # Con esto muestro el total del movimiento como
    # help_text del importe
    def get_form(self, request, obj=None, **kwargs):
        if obj:
            help_texts = {'importe': 'Total $%s' % (obj.importe * obj.cantidad_cuotas)}
        else:
            initial = dict(request.GET.items())
            help_texts = {}
            try:
                help_texts['concepto'] = f'Valor posible: {initial["concepto_no_encontrado"]}'
            except KeyError:
                pass
            try:
                if distutils.util.strtobool(initial['es_moneda_extranjera']):
                    cotizacion_dolar = moneda_extranjera.functions.cotizar()
                    help_texts['importe'] = f'Valor de la moneda extranjera {cotizacion_dolar["valor"]}'
            except KeyError as err:
                # No invocado desde el boton "Cargar", en Procesar datos Naranja
                pass
        kwargs.update({'help_texts': help_texts})
        return super(MovimientoAdmin, self).get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj:  # editing an existing object
            return self.readonly_fields + (  # 'titular',
                                             # 'entidad_financiera',
                                             # 'concepto',
                                             # 'cupon',
                                           'mes_inicio',
                                           'cantidad_cuotas',
                                           'importe')
        return self.readonly_fields


class PermanenteListFilter(admin.SimpleListFilter):
    title = 'uso'

    parameter_name = 'actual'

    def lookups(self, request, model_admin):
        return (
            ('actual', 'Actual'),
        )
    # Intento fallido por hacer que aparezca por defecto la opción en lugar de Todos, seguir viendo aca:
    # https://stackoverflow.com/questions/851636/default-filter-in-django-admin
    # def choices(self, cl):
    #     for lookup, title in self.lookup_choices:
    #         yield {
    #             'selected': self.value() == lookup,
    #             'query_string': cl.get_query_string({
    #                 self.parameter_name: lookup,
    #             }, []),
    #             'display': title,
    #         }

    def queryset(self, request, queryset):
        if self.value() == 'actual':
            nombre_entidad_financiera = 'Tarjeta Naranja'
            mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera).strftime('%Y-%m')

            return queryset.filter(Q(mes_fin=0) |
                                   (Q(mes_inicio__lte=mes_actual) & Q(mes_fin__gte=mes_actual))
                                   )


class InputFilter(admin.SimpleListFilter):
    template = 'admin/input_filter.html'

    # TODO: Se podría implementar una máscara con jquery para reforzar la validación,
    #  ya que el INPUT de HTML5 sólo valida después de confirmar.
    #  Se puede usar esto: https://github.com/RobinHerbots/Inputmask
    #  o esto quizás: https://github.com/caioariede/django-input-mask

    def lookups(self, request, model_admin):
        # Dummy, required to show the filter.
        return (),

    def choices(self, changelist):
        # Grab only the "all" option.
        all_choice = next(super().choices(changelist))
        all_choice['query_parts'] = (
            (k, v)
            for k, v in changelist.get_filters_params().items()
            if k != self.parameter_name
        )
        yield all_choice

    def queryset(self, request, queryset):
        pass


class UUIDFilter(InputFilter):
    parameter_name = 'mes_anio'
    title = 'año y mes'

    def queryset(self, request, queryset):
        if self.value() is not None:
            mes_anio = self.value()
            print(f'qs: {queryset}')
            return queryset.filter(mes=mes_anio)


class GastoListFilter(admin.SimpleListFilter):
    title = 'uso'

    parameter_name = 'actual'

    def lookups(self, request, model_admin):
        return (
            ('actual', 'Actual'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'actual':
            nombre_entidad_financiera = 'Tarjeta Naranja'
            mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera).strftime('%Y-%m')
            return queryset.filter(mes=mes_actual)


@admin.register(Permanente)
class PermanenteAdmin(admin.ModelAdmin):
    model = Permanente
    list_display = (
        'concepto',
        'cupon',
        'entidad_financiera',
        'mes_inicio',
        'mes_fin',
        'importe',
        'acciones',
    )
    readonly_fields = (
        'acciones',
    )
    fields = (
        'concepto',
        'cupon',
        'entidad_financiera',
        'mes_inicio',
        'mes_fin',
        'importe',
    )
    search_fields = ('concepto__descripcion',)
    list_filter = (
        PermanenteListFilter,
    )

    def process_finalizar(self, request, permanente_id):
        return self.process_action(
            request=request,
            permanente_id=permanente_id,
            action_form=FinalizarForm,
            action_title='Finalizar',
        )

    def process_reactivar(self, request, permanente_id):
        return self.process_action(
            request=request,
            permanente_id=permanente_id,
            action_form=ReactivarForm,
            action_title='Reactivar',
        )

    def process_action(
            self,
            request,
            permanente_id,
            action_form,
            action_title
    ):
        permanente = self.get_object(request, permanente_id)
        if request.method != 'POST':
            form = action_form()
        else:
            form = action_form(request.POST)
            if form.is_valid():
                try:
                    form.save(permanente, request.user)
                except Exception as e:
                    # If save() raised, the form will a have a non
                    # field error containing an informative message.
                    print(e)
                else:
                    self.message_user(request, 'Success')
                    # url = reverse(
                    #     'admin:gastos_permanente_changelist',
                    #     args=[permanente.pk],
                    #     current_app=self.admin_site.name,
                    # )
                    changelist_permanente_url = reverse(
                        'admin:gastos_permanente_changelist',
                        current_app=self.admin_site.name,
                    )
                    return HttpResponseRedirect(changelist_permanente_url)

        # Originalmente intenté hacerlo en forms.py al inicializar el form, pero generaba problemas con el request.
        if permanente:
            mes_anterior_al_actual = timezone.localdate() - relativedelta(months=1)
            if mes_anterior_al_actual < datetime.strptime(str(permanente.mes_inicio), '%Y-%m').date():
                mes_fin_sugerido = permanente.mes_inicio
            else:
                mes_fin_sugerido = mes_anterior_al_actual

            form.fields['mes_fin'].initial = mes_fin_sugerido

        template = 'admin/permanente/permanente_action.html'

        context = self.admin_site.each_context(request)
        context['opts'] = self.model._meta
        context['form'] = form
        context['permanente'] = permanente
        context['title'] = action_title
        return TemplateResponse(
            request,
            template,
            context,
        )

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            url(
                r'^(?P<permanente_id>.+)/finalizar/$',
                self.admin_site.admin_view(self.process_finalizar),
                name='permanente-finalizar',
            ),
            url(
                r'^(?P<permanente_id>.+)/reactivar/$',
                self.admin_site.admin_view(self.process_reactivar),
                name='permanente-reactivar',
            ),
        ]
        return custom_urls + urls

    def acciones(self, obj):
        if not obj.mes_fin:
            boton_finalizar = '<a class="button" href="{}">Finalizar</a>&nbsp;'
            boton_reactivar = '<a class="button" href="{}">Re-activar</a>'
        else:
            boton_finalizar = '<a class="button not-allowed" disabled>Finalizar</a>&nbsp;'
            boton_reactivar = '<a class="button" disabled>Re-activar</a>'
        # return format_html(
        #     '<a class="button" href="{}">Finalizar</a>&nbsp;'
        #     '<a class="button" href="{}">Re-activar</a>',
        #     # reverse('admin:gastos_permanente_change', args=[obj.pk]),
        #     # reverse('admin:gastos_permanente_change', args=[obj.pk]),
        #     reverse('admin:permanente-finalizar', args=[obj.pk]),
        #     reverse('admin:permanente-reactivar', args=[obj.pk]),
        # )
        return format_html(
            boton_finalizar +
            boton_reactivar,
            reverse('admin:permanente-finalizar', args=[obj.pk]),
            reverse('admin:permanente-reactivar', args=[obj.pk]),
        )
    acciones.short_description = 'Acciones'
    acciones.allow_tags = True


@admin.register(Gasto)
class GastoAdmin(admin.ModelAdmin):  # ModelAdminTotals):
    model = Gasto
    list_display = (
        'mov_concepto',
        'cupon',
        'mes',
        'importe',
        'plan_zeta',
    )
    fields = (
        'movimiento',
        'cupon',
        'mes',
        'importe',
        'observacion',
    )
    # list_totals = [('importe', lambda field: Coalesce(Sum(field), 0)),]
    list_totals = [('importe', Sum)]
    search_fields = ('movimiento__concepto__descripcion',)
    list_filter = (
        'mes',
        'movimiento__titular__nombre',
        'movimiento__plan_zeta',
        GastoListFilter,
        UUIDFilter,
    )

    def plan_zeta(self, obj):
        return obj.movimiento.plan_zeta
    plan_zeta.short_description = 'Plan zeta'
    plan_zeta.admin_order_field = 'movimiento__plan_zeta'

    def mov_concepto(self, obj):
        return obj.__str__()
    #     return obj.movimiento.concepto.descripcion
    mov_concepto.short_description = 'Concepto'
    mov_concepto.admin_order_field = 'movimiento__concepto__descripcion'

    # mensaje = 'Para eliminar todos los gastos de un movimiento use el botón eliminar entrando a uno de los gastos.'
    # all_warning_messages_content = [msg.message for msg in list(messages.get_messages(request)) if
    #                                 msg.level_tag == 'warning']
    # if mensaje not in all_warning_messages_content:
    #     messages.add_message(request, messages.WARNING, mensaje)

    def has_delete_permission(self, request, obj=None):
        # Al editar un gasto advertir de la eliminación del movimiento entero
        selected = request.POST.getlist(admin.helpers.ACTION_CHECKBOX_NAME)
        if obj is not None:  # obj guarda el registro, si es None es un Create, sino un Update
            if selected:
                mensaje = 'Se eliminarán únicamente el/los gasto/s seleccionados.'
            else:
                mensaje = '¡ADVERTENCIA! Al eliminar este gasto se eliminará tambien ' \
                          'el movimiento asociado y TODOS los gastos relacionados'
            all_warning_messages_content = [msg.message for msg in list(messages.get_messages(request)) if
                                            msg.level_tag == 'warning']
            if mensaje not in all_warning_messages_content:
                messages.add_message(request, messages.WARNING, mensaje)
            # self.message_user(request, 'Pack contiene un valor')
            return True  # Poner esto en False si queremos que no aparezca el botÃ³n eliminar
        return super(GastoAdmin, self).has_delete_permission(request, obj)


@admin.register(Cierre)
class CierreAdmin(admin.ModelAdmin):
    model = Cierre
    extra = 1
    max_num = 1

    # def has_add_permission(self, request):
    #     # if there's already an entry, do not allow adding
    #     count = Cierre.objects.all().count()
    #     if count == 0:
    #       return True
    #     return False


@admin.register(ResumenTNTemp)
class ResumenTNTempAdmin(admin.ModelAdmin):  # ModelAdminTotals):
    model = ResumenTNTemp
    list_display = (
        'mes',
        'tipo',
        'fecha',
        'tarjeta',
        'cupon',
        'detalle',
        'cuota',
        'importe',
        'moneda',
        'considerar',
    )


@admin.register(GastoPermanente)
class GastoPermanenteAdmin(admin.ModelAdmin):  # ModelAdminTotals):
    model = GastoPermanente
    list_display = (
        'detalle',
        'campo_pdf',
    )


admin.site.register(Titular)
admin.site.register(Entidad_financiera)
admin.site.register(NombreParcial)
admin.site.register(Configuracion)
admin.site.register(GastoExcluidoResumen)
admin.site.register(ColorElemento)
