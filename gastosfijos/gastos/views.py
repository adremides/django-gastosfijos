from django.http import HttpResponse
from django.template import loader
from .models import Permanente, Gasto, Cierre, Titular, ResumenTNTemp, Concepto, Entidad_financiera
from .models import NombreParcial, GastoPermanente, Configuracion, GastoExcluidoResumen
from django.utils import timezone
from django.db.models import Sum, Value, CharField, F, Func, Q, DecimalField
from django.db.models.functions import Coalesce
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY
from django.template.defaultfilters import date as formatear_fecha
from django.utils.safestring import mark_safe

from .pdf import TarjetaNaranja
from babel.numbers import parse_decimal
from datetime import datetime

import itertools
import collections
import decimal

import django_tables2 as dt2
from django_tables2 import RequestConfig, A
from .tables import GastoTable, Tablas, TablasDict

from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.contrib import messages

from .comparaciones import ComparacionTarjetaNaranja
import moneda_extranjera


@login_required
def index(request):
    nombre_entidad_financiera = 'Tarjeta Naranja'

    mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera)

    configuracion = Configuracion.obtener_configuracion()

    cotizacion_dolar = moneda_extranjera.functions.cotizar()

    # Los permanentes actuales serían los que tiene fecha de inicio del mes en curso
    # y los que tienen cualquier fecha de inicio pero no tienen una fecha final.
    # permanentes_mes_actuales = Permanente.objects.filter(entidad_financiera__nombre=nombre_entidad_financiera)
    permanentes_mes_actuales = Permanente.objects.all()
    permanentes_mes_actuales = permanentes_mes_actuales.filter(
        Q(mes_fin=0) | (Q(mes_inicio__lte=mes_actual) & Q(mes_fin__gte=mes_actual)))
    importe_total_permanentes = permanentes_mes_actuales.aggregate(
        total=Coalesce(Sum('importe'), 0, output_field=DecimalField())
    )
    imp_total_perm_dividido = importe_total_permanentes['total'] / decimal.Decimal.from_float(2.0)

    # gastos_mes_actual = Gasto.objects.filter(movimiento__entidad_financiera__nombre=nombre_entidad_financiera)
    gastos_mes_actual = Gasto.objects.all()
    gastos_mes_actual = gastos_mes_actual.filter(mes=mes_actual)
    gastos_mes_actual = gastos_mes_actual.exclude(movimiento__plan_zeta=True)
    importe_total_gastos = gastos_mes_actual.aggregate(
        total=Coalesce(Sum('importe'), 0, output_field=DecimalField())
    )

    total_mes_por_titular = gastos_mes_actual.values('movimiento__titular__nombre').annotate(
        total=Func(
            Sum('importe'), function="ROUND", template='%(function)s(%(expressions)s, 2)'
        )
    )
    lista_tot_mes_por_titular = list(total_mes_por_titular)

    for titular in lista_tot_mes_por_titular:
        titular['titular'] = titular.pop('movimiento__titular__nombre')  # cambiar nombre del key
        titular['total'] = titular.pop('total')  # esto es para que 'total' quede segundo
        titular['total'] += imp_total_perm_dividido

    total_mes_actual = importe_total_permanentes['total'] + importe_total_gastos['total']  # Tipo: Decimal

    template = loader.get_template('gastos/index.html')

    context = {
        'lista_tot_mes_por_titular': lista_tot_mes_por_titular,
        'total_mes_actual': total_mes_actual,
        'cotizar_habilitado': configuracion.cotizar,
        'cotizacion_dolar': cotizacion_dolar,
    }

    return HttpResponse(template.render(context, request))


@login_required
def proximos(request):
    nombre_entidad_financiera = 'Tarjeta Naranja'
    configuracion = Configuracion.obtener_configuracion()
    meses_antes = configuracion.detalle_meses_antes
    meses_despues = configuracion.detalle_meses_despues
    # print("meses antes: " + str(meses_antes))
    # print("meses_despues antes: " + str(meses_despues))

    mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera).replace(day=1)
    # print(f'mes_actual = {mes_actual} tipo {type(mes_actual)}')

    mes_desde = mes_actual + relativedelta(months=meses_antes * -1)
    mes_hasta = mes_actual + relativedelta(months=meses_despues)

    titulares = Titular.objects.all()

    lista_meses = [dt for dt in rrule(MONTHLY, dtstart=mes_desde, until=mes_hasta)]
    columnas_extra_por_titular = {'todos_los_titulares': []}

    lista_totales_por_titular = []

    total_general_por_mes = collections.Counter()

    tablas_por_titular = {}
    for i_Z, titular in enumerate(titulares):
        gastos_y_permanentes_del_titular = titular.get_gastos_y_permanentes_por_entidad_financiera()
        # qs = Gasto.objects.all().filter(movimiento__titular__nombre=titular.nombre).exclude(movimiento__plan_zeta=True)
        # qs = qs.filter(movimiento__entidad_financiera__nombre=nombre_entidad_financiera)
        #
        # gastos_y_permanentes = lista_gastos_por_fecha(qs, mes_desde, mes_hasta)
        gastos_entidad_financiera = []

        for ef in gastos_y_permanentes_del_titular:
            gastos_entidad_financiera.append(
                lista_gastos_por_fecha(ef["gastos"], ef["permanentes"], mes_desde, mes_hasta)
            )

        # ToDo: Esto habría que ver cómo hacerlo, porque ahora tendría que
        #  haber un total por entidad_financiera
        # # guardo los totales de cada fecha por titular para armar una tabla con los totales después
        # lista_totales_por_titular.append(gastos_y_permanentes['totales'])
        # # para que el template reconozca cada registro uso 'gasto' en lugar de 'titular'
        # lista_totales_por_titular[i].update({'gasto': titular.nombre})
        # # por compatibilidad para que al consultar el tipo de fila a colorear no de error
        # lista_totales_por_titular[i].update({'tipo': 'total'})
        # # por ahora por compatibilidad para que al consultar el bg_color de la fila no de error
        # lista_totales_por_titular[i].update({'bg_color': ''})
        # # total_general_por_mes es un Counter() que suma según las  del diccionario
        # total_general_por_mes.update(gastos_y_permanentes['totales'])
        # # Agrego esto para que no me de error al buscar el campo en tables.py->GastoTable->args
        # lista_totales_por_titular[i].update({'pk_movimiento': 0})  # Habría que mejorar esto, pero funciona
        # ToDo FIN

        # Agrego una tabla por titular
        # tablas_por_titular[titular.nombre] = [
        #     GastoTable(
        #         gastos_y_permanentes['detalles'],
        #         # extra_columns=columnas_extra_por_titular[titular.nombre])
        #         extra_columns=gastos_y_permanentes['columnas_extra']
        #     ),
        #     GastoTable(
        #         gastos_y_permanentes['detalles'],
        #         # extra_columns=columnas_extra_por_titular[titular.nombre])
        #         extra_columns=gastos_y_permanentes['columnas_extra']
        #     )
        # ]

        # FUNCIONA PERO SE DEMORA MUCHÍSIMO!!!!!!!!!!!
        tablas_por_titular[titular.nombre] = []
        for gastos_y_permanentes in gastos_entidad_financiera:
            # Agregar una tabla a un titular
            if gastos_y_permanentes['detalles']:
                tablas_por_titular[titular.nombre] += [
                    GastoTable(
                        gastos_y_permanentes['detalles'],
                        # extra_columns=columnas_extra_por_titular[titular.nombre])
                        extra_columns=gastos_y_permanentes['columnas_extra']
                    )
                ]

        # RequestConfig(request, paginate=False).configure(lista_tablas[i][1])

    # genero las columnas extra para los totales generales
    for mes in lista_meses:
        mes_formateado = mes.strftime('%Y-%m')
        total = total_general_por_mes[mes_formateado]
        columnas_extra_por_titular['todos_los_titulares'].append([
            mes.strftime('%Y-%m'),
            dt2.Column(
                footer=total,
                attrs={'td': {'tipo': atributo_td}}
            )])

    # Agrego una tabla con los totales por mes de cada titular
    tablas_por_titular['Totales por titular'] = [
        GastoTable(
            lista_totales_por_titular,
            extra_columns=columnas_extra_por_titular[
                'todos_los_titulares'
            ]
        )
    ]

    template_name = "gastos/tabla.html"

    return render(request, template_name, {"tablas_por_titular": tablas_por_titular})


@login_required
def tabla(request):
    # table = Gasto.objects.all()
    mes_actual = timezone.localdate().strftime('%Y-%m')
    mes_proximo = (timezone.localdate() + relativedelta(months=1)).strftime('%Y-%m')
    campos = ({mes_actual: F('importe'), 'gasto': F('movimiento__concepto__descripcion')})
    prueba_mes_actual = Gasto.objects.filter(mes=mes_actual)
    prueba_mes_proximo = Gasto.objects.filter(mes=mes_proximo)
    juntos = prueba_mes_actual | prueba_mes_proximo
    c = ({str(F('mes')): F('importe'), 'gasto': F('movimiento__concepto__descripcion')})
    juntos = juntos.annotate(**c)
    # juntos = juntos.values('gasto', mes_actual, mes_proximo)
    # juntos = juntos.values('gasto', 'mes', 'importe')
    # print(juntos)
    gastos_mes_actual = Gasto.objects.filter(mes=mes_actual) \
        .annotate(**campos) \
        .values('gasto', mes_actual)

    qs = gastos_mes_actual  # | gastos_mes_proximo
    table = GastoTable(qs, extra_columns=((mes_actual, dt2.Column()), (mes_proximo, dt2.Column()),))
    template_name = "gastos/tabla.html"
    RequestConfig(request, paginate=False).configure(table)
    return render(request, template_name, {"table": table})


@login_required
def tabla_test(request):
    # Los permanentes actuales serían los que tiene fecha de inicio del mes en curso y los que tienen cualquier fecha de
    # inicio pero no tienen una fecha final.
    permanentes_mes_actuales = Permanente.objects.filter(
        Q(mes_fin=0) | Q(mes_inicio=timezone.localdate().strftime('%Y-%m')))
    importe_total_permanentes = permanentes_mes_actuales.aggregate(
        total=Coalesce(Sum('importe'), 0, output_field=DecimalField())
    )
    imp_total_perm_dividido = importe_total_permanentes['total'] / decimal.Decimal.from_float(2.0)

    gastos_mes_actual = Gasto.objects.filter(mes=timezone.localdate().strftime('%Y-%m'))
    # consulta = gastos_mes_actual.values('movimiento__titular__nombre', 'mes', 'importe')
    consulta = gastos_mes_actual.annotate(titular=F('movimiento__titular__nombre'), fecha=F('mes')).values("titular",
                                                                                                           "fecha",
                                                                                                           "importe")
    test = timezone.localdate() + relativedelta(months=-1)
    # para que muestre el mes en español uso date de django.template.defaultfilters en lugar de strftime
    test2 = formatear_fecha(test, "F Y")
    test = test.strftime('%B-%Y')
    print("test: ")
    print(test)
    print(test2)
    print("------------------------------------------------")

    print("consulta:")
    print(consulta)
    print("------------------------------------------------")

    # gastos_proximos = Gasto.objects.filter(mes=timezone.localdate().strftime('%Y-%m') + timezone.timedelta(months=1))
    importe_total_gastos = gastos_mes_actual.aggregate(
        total=Coalesce(Sum('importe'), 0, output_field=DecimalField())
    )

    print("Gastos mes actual: ")
    print(gastos_mes_actual)
    print("------------------------------------------------")

    total_mes_por_titular = gastos_mes_actual.values('movimiento__titular__nombre').annotate(
        total=Func(Sum('importe'), function="ROUND", template='%(function)s(%(expressions)s, 2)')
    )
    lista_tot_mes_por_titular = list(total_mes_por_titular)

    # print(total_mes_por_titular)
    print("Lista total por titular del mes: ")
    print(lista_tot_mes_por_titular)
    print("------------------------------------------------")

    for titular in lista_tot_mes_por_titular:
        # print("%s: %s" % (titular['movimiento__titular__nombre'], titular['total'])) # titular, importe
        titular['titular'] = titular.pop('movimiento__titular__nombre')  # cambiar nombre del key
        titular['total'] = titular.pop('total')  # esto es para que 'total' quede segundo
        titular['total'] += imp_total_perm_dividido

    total_mes_actual = importe_total_permanentes['total'] + importe_total_gastos['total']  # Tipo: Decimal

    template = loader.get_template('gastos/proximos.html')

    context = {'lista_tot_mes_por_titular': lista_tot_mes_por_titular,
               'total_mes_actual': total_mes_actual, }

    return HttpResponse(template.render(context, request))


@login_required
def dict_test(request):
    lista_tablas = []
    prueba = {}
    titulares = Titular.objects.all()
    for titular in titulares:
        prueba[titular.nombre] = []
        prueba[titular.nombre].append(['2019-01', dt2.Column(footer="prueba")])
        prueba[titular.nombre].append(['2019-02', dt2.Column(footer=decimal.Decimal("100.50"))])

    columnas_extra = prueba['Nosotros'].copy()

    datos = [{'2019-01': decimal.Decimal('631.68'), 'gasto': 'MercadoPago MotoG4+'},
             {'2019-01': decimal.Decimal('643.74'), 'gasto': 'Óptica Humor'},
             {'2019-01': decimal.Decimal('432.50'), 'gasto': 'HDD externo ShowNet'},
             {'2019-01': decimal.Decimal('654.20'), 'gasto': 'Silla Farfalla'},
             {'2019-01': decimal.Decimal('300.00'), 'gasto': 'Carros Vaqueria'},
             {'2019-01': decimal.Decimal('805.00'), 'gasto': 'Las Grutas S.A IDA BOLSÓN'},
             {'2019-02': decimal.Decimal('805.00'), 'gasto': 'Las Grutas S.A IDA BOLSÓN'},
             {'2019-01': decimal.Decimal('933.98'), 'gasto': 'Las Grutas S.A VUELTA BOLSÓN'},
             {'2019-02': decimal.Decimal('933.98'), 'gasto': 'Las Grutas S.A VUELTA BOLSÓN'},
             {'2019-01': decimal.Decimal('270.00'), 'gasto': 'NIC'},
             {'2019-02': decimal.Decimal('270.00'), 'gasto': 'NIC'},
             {'2019-01': decimal.Decimal('1450.00'), 'gasto': 'Folletos'},
             {'2019-01': decimal.Decimal('-1450.00'), 'gasto': 'Folletos'}]
    # print(datos)
    lista_tablas.append([
        "Título",
        Tablas(datos, extra_columns=columnas_extra)
    ]
    )
    RequestConfig(request, paginate=False).configure(lista_tablas[0][1])
    template_name = "gastos/tablas.html"

    return render(request, template_name, {"lista_tablas": lista_tablas})


def atributo_td(**kwargs):
    """
    Devuelve el valor del atributo 'tipo' para cada celda de la tabla. Si la celda está en la columna del mes
    que coincide con el actual, devuelve el tipo de celda (gasto, permanente o ninguno) y 'actual'
    Las columnas que no tienen 'record' son las de los totales por titular.

    :param kwargs:
    Diccionario con los datos que obtiene django-tables2.Column()
    :return:
    tipo (gasto, permanente o ninguno) + ' actual' o None
    """
    # print(kwargs)
    # if not keys_exists(kwargs, 'record'):
    #     print(kwargs['bound_column'])

    nombre_entidad_financiera = 'Tarjeta Naranja'

    mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera).strftime('%Y-%m')
    # print(kwargs['bound_column']==mes_actual)
    tipo = kwargs['record']['tipo'] if keys_exists(kwargs, 'record') else ""
    return tipo + ' actual' if str(kwargs['bound_column']) == mes_actual else None


def style_td(**kwargs):
    """
    Si la celda está en la columna del mes que coincide con el actual, devuelve un style que oscurece la celda.
    Las columnas que no tienen 'record' son las de los totales por titular.

    :param kwargs:
    Diccionario con los datos que obtiene django-tables2.Column()
    :return:
    tipo (gasto, permanente o ninguno) + ' actual' o None
    """
    # print(kwargs)
    # if not keys_exists(kwargs, 'record'):
    #     print(kwargs['bound_column'])

    darken_background = 'background: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1))'

    nombre_entidad_financiera = 'Tarjeta Naranja'

    mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera).strftime('%Y-%m')
    # print(kwargs['bound_column']==mes_actual)
    tipo = kwargs['record']['tipo'] if keys_exists(kwargs, 'record') else ""
    return darken_background if str(kwargs['bound_column']) == mes_actual else ''


def lista_gastos_por_fecha(qs_gastos, qs_permanentes, fecha_desde, fecha_hasta):
    meses = [dt for dt in rrule(MONTHLY, dtstart=fecha_desde, until=fecha_hasta)]
    lista_consultas = []

    cantidad_titulares = Titular.objects.count()

    lista_gastos = {'detalles': [], 'totales': {}, 'columnas_extra': []}

    for mes in meses:
        mes_formateado = mes.strftime('%Y-%m')

        # CONSULTAS GASTOS DEL MES
        campos = ({
            mes_formateado: F('importe'),
            'gasto': F('movimiento__concepto__descripcion'),
            'nro_cupon': F('cupon'),
            'detalle': F('observacion'),
            'tipo': Value('gasto', CharField()),  # una columna del tipo CharField que contendrá fijo 'gasto'
            # 'bg_color': Value('#afc3cb', CharField()),
            'bg_color': F('movimiento__entidad_financiera__color__gasto'),
            'pk_' + mes_formateado: F('pk'),
            'pk_movimiento': F('movimiento__pk'),
        })
        # print('campos.keys(): ', campos.keys())
        gastos_mes = qs_gastos.filter(mes=mes_formateado) \
            .annotate(**campos) \
            .values('gasto', mes_formateado, 'nro_cupon', 'detalle', 'tipo', 'bg_color', 'pk_' + mes_formateado, 'pk_movimiento')
        total_gastos = gastos_mes.aggregate(
            total=Coalesce(Sum('importe'), 0, output_field=DecimalField())
        )

        # CONSULTA PERMANENTES DEL MES
        campos_permanentes = ({
            mes_formateado: F('importe'),
            'gasto': F('concepto__descripcion'),
            'bg_color': F('entidad_financiera__color__permanente'),
            'pk_'+mes_formateado: F('pk')
        })
        permanentes_mes = qs_permanentes.filter(
            (Q(mes_fin=0) & Q(mes_inicio__lte=mes_formateado)) |
            (Q(mes_inicio__lte=mes_formateado) & Q(mes_fin__gte=mes_formateado))
        ) \
            .annotate(**campos_permanentes) \
            .values('gasto', mes_formateado, 'bg_color', 'pk_' + mes_formateado)

        importe_total_permanentes = permanentes_mes.aggregate(
            total=Coalesce(Sum('importe'), 0, output_field=DecimalField())
        )
        imp_total_perm_dividido = importe_total_permanentes['total'] / decimal.Decimal.from_float(2.0)

        # Diccionario con totales por mes
        if total_gastos['total']:
            # si hay gastos le suma los impuestos (permanentes)
            lista_gastos['totales'][mes_formateado] = (total_gastos['total'] + imp_total_perm_dividido).normalize()
        else:
            # sino solo trae los impuestos (permanentes), en teoría estos siempre tiene que haber
            lista_gastos['totales'][mes_formateado] = imp_total_perm_dividido.normalize()

        # Esto genera la lista de columnas extra (los diferentes meses) de cada uno de los gastos.
        # El pie de la tabla para cada columna contendrá el total de cada uno de los meses.
        # lista_gastos['columnas_extra'].append([
        #     mes_formateado, dt2.Column(
        #         footer=lista_gastos['totales'][mes_formateado],
        #         attrs={'td': {'tipo': atributo_td}}
        #     )
        # ])
        lista_gastos['columnas_extra'].append([
            mes_formateado,
            dt2.LinkColumn(
                'admin:gastos_gasto_change',
                args=[A('pk_' + mes_formateado)],
                footer=lista_gastos['totales'][mes_formateado],
                attrs={
                    'td': {
                        'tipo': atributo_td,
                        'style': style_td
                    }
                }
            )
        ])

        # Formateo de los permanentes para que queden igual que los gastos
        lista_permanentes_mes = list(permanentes_mes)
        for indx, item in enumerate(lista_permanentes_mes):
            primer_key = next(iter(item))
            # ToDo: Deberían ser la cantidad de titulares que usan esa entidad_financiera
            item[primer_key] /= cantidad_titulares  # los permanentes son compartidos entre la cantidad de titulares
            # item['gasto'] = '_' + item['gasto'] # esto era para que vayan a lo último, ahora no hace falta
            # porque ordena por nro_cupon

            # agrego los siguientes campos para compatibilizar ambas tablas y poderlas recorrer como una sola
            # item['nro_cupon'] = 'perm' + str(index) # permite separar aunque tengan el mismo nombre
            # Lo anterior se cambió porque los juntaba aunque no fueran el mismo cuando cambiaban de un mes a otro
            item['nro_cupon'] = obtener_primeras_letras(item['gasto'])[0:5]
            item['detalle'] = None
            item['tipo'] = 'permanente'
            # item['bg_color'] = '#EFDD8D',  # Si manda el color a "style" pero lo manda como una tupla y no funciona
            item['pk_movimiento'] = 0

        # Agrego en la lista cada uno de los tipos de gastos
        lista_consultas.append(gastos_mes)
        lista_consultas.append(lista_permanentes_mes)

    # AGRUPAR LOS DATOS EN UNA SOLA LISTA DE DICCIONARIOS
    # Recorre el queryset agrupando los gastos para que aparezcan las distintas fechas en una sola fila
    # Esto hace que un queryset así:
    # [<QuerySet [{'2018-10': Decimal('100.00'), 'gasto': 'prueba'},
    #             {'2018-11': Decimal('100.00'), 'gasto': 'prueba'},
    #             {'2018-10': Decimal('50.00'), 'gasto': 'otro'}
    #            ]
    # ]
    # Se convierta en una lista así:
    # [{'2018-10': Decimal('100.00'), 'gasto': 'prueba', '2018-11': Decimal('100.00')},
    #  {'2018-10': Decimal('50.00'), 'gasto': 'otro'}
    # ]
    # ACTUALIZACION: Ahora agrupa por cupon, dado que al hacerlo por nombre si estaba repetido cuando lo ordenaba y
    # tenía más de una fecha, iban intercaladas y de esta forma siempre consideraba que era distinto y los separaba.
    lit = itertools.chain(*lista_consultas)
    # lst = sorted(l, key=lambda x: x['gasto'])
    lst = sorted(lit, key=lambda x: x['nro_cupon'])

    gastos_max_mes = []  # ej: [{'1131': '2019-05} {'15': '2019-02'}]

    # for llave, valor in itertools.groupby(lst, key=lambda x: x['gasto']):
    for llave, valor in itertools.groupby(lst, key=lambda x: x['nro_cupon']):
        fila_gasto = {}

        for elemento in valor:
            # creo un diccionario con solamente el importe de la fecha
            solo_fecha = elemento.copy()
            del solo_fecha['gasto']
            del solo_fecha['detalle']
            del solo_fecha['nro_cupon']
            key = next(iter(solo_fecha))  # esto obtiene la llave que es la fecha (ej: 2019-01)

            # Agrego en una segunda línea el detalle para cada gasto, si lo tiene
            if elemento['detalle']:
                elemento['gasto'] = mark_safe(elemento['gasto'] + '<br>' + elemento['detalle'])

            # verifico que no exista ya la fecha agregada al gasto (o sea, que se haya cargado un mismo gasto
            # con distinto importe para la misma fecha)
            if keys_exists(fila_gasto, key):
                # si existe ya una, lo agrego como un gasto a parte
                lista_gastos['detalles'].append(elemento)
            else:
                # sino lo agrego al diccionario del gasto que ya estaba
                fila_gasto.update(elemento)

        # Crear diccionario con la fecha más grande por gasto
        # Solo necesito los key que tienen la fecha, pero como no puedo traer el valor que estoy buscando
        # porque no es fijo ('año-mes') entonces elimino todos los que no necesito.
        f = fila_gasto.copy()
        del f['gasto']
        del f['nro_cupon']
        del f['detalle']
        del f['tipo']
        # Elimino todos los pk (id del registro)
        for k in fila_gasto.keys():
            if k.startswith('pk_'):
                del f[k]
        d = {fila_gasto['nro_cupon']: max(f.keys())}  # ej: {'1131': '2019-05}
        if fila_gasto['tipo'] == 'gasto':
            gastos_max_mes.append(d)

        lista_gastos['detalles'].append(fila_gasto)

    # Agrego un elemento {'orden': index} para los gastos (son los que tienen nro_cupon) en
    # lista_gastos['detalles'] según el orden de gastos_ordenados_por_fecha_maxima
    gastos_ordenados_por_fecha_maxima = sorted(gastos_max_mes, key=lambda x: x[next(iter(x))], reverse=True)
    for i, gasto in enumerate(gastos_ordenados_por_fecha_maxima):
        nro_cupon = next(iter(gasto))
        for elemento in lista_gastos['detalles']:
            if elemento['nro_cupon'] == nro_cupon:
                elemento['orden'] = i
            if elemento['tipo'] == 'permanente':
                elemento['orden'] = 99999

    # Ordena solamente si se pudo establecer un orden. Los permanentes no se ordenan, por eso
    # esto es necesario solo si hay gastos. Si no hay gastos, gastos_ordenados_por_fecha_maxima estará vacío.
    if len(gastos_ordenados_por_fecha_maxima) > 0:
        # Ordeno los gastos de la tabla según el orden guardado anteriormente en gastos_ordenados_por_fecha_maxima
        lista_gastos_ordenada = {'detalles': sorted(lista_gastos['detalles'], key=lambda x: x['orden'])}
        lista_gastos['detalles'] = lista_gastos_ordenada['detalles']

    return lista_gastos


def keys_exists(element, *keys):
    """
    Check if *keys (nested) exists in `element` (dict).
    """
    if type(element) is not dict:
        raise AttributeError('keys_exists() expects dict as first argument.')
    if len(keys) == 0:
        raise AttributeError('keys_exists() expects at least two arguments, one given.')

    _element = element
    for key in keys:
        try:
            _element = _element[key]
        except KeyError:
            return False
    return True


def obtener_primeras_letras(texto):
    palabras = texto.split()
    letras = [palabra[0] for palabra in palabras]
    return "".join(letras)


@login_required
def extraer_resumen_naranja(request):
    template = 'gastos/subir_resumen.html'

    if request.method == 'POST':
        # myFile = request.FILES.get('resumen_naranja')
        try:
            archivo_pdf = request.FILES['resumen_naranja'].file
            pdf_naranja = TarjetaNaranja(archivo_pdf)
            ResumenTNTemp.objects.all().delete()

            guardar_datos_extraidos(pdf_naranja.datos_nosotros, pdf_naranja)
            guardar_datos_extraidos(pdf_naranja.datos_ma, pdf_naranja)
            guardar_impuestos(pdf_naranja)
            messages.add_message(request, messages.INFO, 'Datos cargados con éxito')
        except Exception as e:
            messages.add_message(request, messages.ERROR, 'No fue posible cargar los datos: %s' % (str(e)))

    return render(request, template)


@login_required
def procesar_datos_naranja(request):
    ctn = ComparacionTarjetaNaranja()
    ctn.comparar()

    lista_tablas = []

    # Para cada entrada de diccionario (nombre de la tabla comparativa) hay un diccionario de 3 elementos que
    # contiene el título, las columnas y los datos.
    datos_tablas = generar_datos_tabla(ctn)

    # Para cada uno de los nombres de las tablas comparativas,
    # si la tabla tiene datos, agrego la tabla con sus datos a la variable de context (lista_tablas)
    for nombre_comparativa in datos_tablas:
        if datos_tablas[nombre_comparativa]['datos']:
            lista_tablas.append(
                [datos_tablas[nombre_comparativa]['titulo'],
                 TablasDict(datos_tablas[nombre_comparativa]['datos'],
                            extra_columns=datos_tablas[nombre_comparativa]['columnas'])]
            )

    for i, t in enumerate(lista_tablas):
        RequestConfig(request, paginate=False).configure(lista_tablas[i][1])
    template_name = "gastos/tablas.html"

    factura_gastos = ResumenTNTemp.objects.first()

    mes_actual = Cierre.obtener_mes_actual('Tarjeta Naranja').strftime('%Y-%m')

    if factura_gastos.mes != mes_actual:
        lista_tablas = []

    return render(request, template_name, {'lista_tablas': lista_tablas,
                                           'fecha_factura': factura_gastos.mes,
                                           'periodo_actual': mes_actual})


@login_required
def dummy(request):
    template = loader.get_template('gastos/procesar_datos.html')
    dummy1 = dummy2 = ''
    context = {'dummy1': dummy1,
               'dummy2': dummy2, }
    return HttpResponse(template.render(context, request))


def guardar_datos_extraidos(datos, pdf_naranja):
    nombre_entidad_financiera = 'Tarjeta Naranja'
    gastos_excluidos = GastoExcluidoResumen.objects \
        .values('cupon') \
        .filter(entidad_financiera__nombre=nombre_entidad_financiera)
    cupones_excluidos = gastos_excluidos.values_list('cupon', flat=True)
    for gasto in datos:
        mes = datetime.strptime(pdf_naranja.fecha_vencimiento, '%d/%m/%y').date().strftime('%Y-%m')
        # tipo = establecer_tipo(gasto['detalle'])
        tipo = establecer_tipo(gasto)
        fecha = None if gasto['fecha'] == '--/--/--' else datetime.strptime(gasto['fecha'], '%d/%m/%y').date()
        tarjeta = gasto['tarjeta']
        cupon = gasto['cupon']
        detalle = nombre_parcial(gasto['detalle'])
        cuota = gasto['cuota_plan'][-2:]
        importe = 0 if gasto['importe'] == '' else parse_decimal(gasto['importe'], locale='es_AR')
        moneda = 'P'
        dolares = 0 if gasto['dolares'] == '' else parse_decimal(gasto['dolares'], locale='es_AR')
        bg_color = '#EFDD8D'  # Esto no sé para qué era...
        if dolares > 0:
            importe = 0 if gasto['dolares'] == '' else parse_decimal(gasto['dolares'], locale='es_AR')
            moneda = 'D'
        considerar = not (cupon in cupones_excluidos)
        nuevo_gasto = ResumenTNTemp.objects.create(mes=mes, tipo=tipo, fecha=fecha, tarjeta=tarjeta, cupon=cupon,
                                                   detalle=detalle, cuota=cuota, importe=importe, moneda=moneda,
                                                   considerar=considerar, procesado=False)
        # print(nuevo_gasto)


def guardar_impuestos(pdf_naranja):
    mes = datetime.strptime(pdf_naranja.fecha_vencimiento, '%d/%m/%y').date().strftime('%Y-%m')
    tipo = 'permanente'
    fecha = None
    cuota = None
    tarjeta = None
    moneda = 'P'
    considerar = True

    # Mantenimiento
    cupon = 'MANT'
    detalle = '*COMISION POR MANTENIMIENTO DE CUENTA'
    importe = parse_decimal(pdf_naranja.importe_mantenimiento, locale='es_AR')
    nuevo_gasto = ResumenTNTemp.objects.create(mes=mes, tipo=tipo, fecha=fecha, tarjeta=tarjeta, cupon=cupon,
                                               detalle=detalle, cuota=cuota, importe=importe, moneda=moneda,
                                               considerar=considerar, procesado=False)
    # print(nuevo_gasto)

    # Sellos
    cupon = 'SELLOS'
    detalle = 'Impuesto de Sellos'
    importe = parse_decimal(pdf_naranja.importe_sellos, locale='es_AR')
    if importe > 0:
        nuevo_gasto = ResumenTNTemp.objects.create(mes=mes, tipo=tipo, fecha=fecha, tarjeta=tarjeta, cupon=cupon,
                                                   detalle=detalle, cuota=cuota, importe=importe, moneda=moneda,
                                                   considerar=considerar, procesado=False)
        # print(nuevo_gasto)

    # IVA
    cupon = 'IVA'
    detalle = 'IVA Operaciones Identificadas con *'
    importe = parse_decimal(pdf_naranja.importe_iva, locale='es_AR')
    nuevo_gasto = ResumenTNTemp.objects.create(mes=mes, tipo=tipo, fecha=fecha, tarjeta=tarjeta, cupon=cupon,
                                               detalle=detalle, cuota=cuota, importe=importe, moneda=moneda,
                                               considerar=considerar, procesado=False)
    print(nuevo_gasto)


def establecer_tipo(gasto):
    # Esto busca en la tabla GastoPermanente todas las coincidencias que deberían
    # transformar un gasto de la lista de gastos en un permamente.
    # Si queremos que cuando el detalle de un gasto contenga la palabra "impuesto"
    # este se transforme en un Permanente entonces hay que agregar en la tabla
    # en el campo campo_pdf "detalle" y en el campo detalle "impuesto".
    gastos_permanentes = GastoPermanente.objects.all()
    es_permanente = False
    for permanente in gastos_permanentes:
        es_permanente = es_permanente or permanente.detalle in gasto[permanente.campo_pdf]
    # lista_permanentes = ['GALICIA SEGU 589-012-4055570054832', 'SALDO POR REDONDEO DE PAGO ANTERIOR']
    # for permanente in lista_permanentes:
    #     es_permanente = es_permanente or permanente in gasto['detalle']
    if es_permanente:
        return 'permanente'
    else:
        return 'gasto'


def nombre_parcial(detalle):
    detalle_modificado = detalle
    nombres_parciales = NombreParcial.objects.all()
    for np in nombres_parciales:
        if str(np) in detalle:
            print('se guardó un nombre parcial')
            detalle_modificado = str(np)
    return detalle_modificado


def detalle_bd_nombre(record):
    return record['detalle(bd)']


def generar_datos_tabla(ctn):
    model_change = {
        'Gasto': 'admin:gastos_gasto_change',
        'Permanente': 'admin:gastos_permanente_change',
    }

    # Cantidad de columnas en la tabla
    tipos_tablas = {
        '2col_bd_y_f': [
            ['base de datos', dt2.Column()],
            ['factura', dt2.Column()],
        ],
        'total_importes_diferencia': [
            ['base de datos', dt2.Column()],
            ['factura', dt2.Column()],
            ['diferencia', dt2.Column()],
        ],
        '4col_f': [
            ['cupon(f)', dt2.Column()],
            ['detalle(f)', dt2.Column()],
            # ['detalle(f)', dt2.LinkColumn('admin:gastos_gasto_changelist', text='static text')],
            # ['detalle(f)', dt2.LinkColumn(
            #     'admin:gastos_gasto_changelist', args=[A('detalle(bd)_id')], text=detalle_bd_nombre
            # )],
            # ['detalle(f)', dt2.Column(linkify=('admin:gastos_gasto_changelist', {A('cupon(f)'),}))],
            # ['detalle(f)', dt2.Column(linkify=('admin:gastos_gasto_changelist', {1,}))],
            ['importe(f)', dt2.Column()],
            ['cuotas(f)', dt2.Column()],
            # ['Excluir', dt2.LinkColumn('admin:gastos_gasto_changelist', text='Excluir')],
            # ['Cargar', dt2.LinkColumn('cargar_movimiento', args=[A('datos')], text='Cargar')],
            # ['Cargar', dt2.LinkColumn(
            #     'admin:gastos_movimiento_add', text='Cargar'
            # )],
            # ['Cargar', dt2.LinkColumn(
            #     'admin:gastos_movimiento_add', text='Cargar', args=['?cupon=2690']
            # )],
            # ['Cargar', dt2.LinkColumn('/admin/gastos/movimiento/add/?cupon=2690', text='Cargar')],
            # Como los nombres de las columnas contienen valores inválidos (los paréntesis) para llamarlos desde el
            # template, se usa un custom template tag (filter) que se llama get para obtenerlos y hay que cargarlo en
            # el template con "load get"
            # Al final esto no hace falta, porque bien se puede usar un diccionario en otro campo (datos en este caso)
            # y se puede llamar a los datos de ese diccionario de forma normal en el template record.datos.item.
            ['Acciones', dt2.TemplateColumn(template_name='acciones_factura_tn.html')],
        ],
        '6col_bd_y_f': [
            ['cupon(f)', dt2.Column()],
            ['detalle(f)', dt2.Column()],
            ['importe(f)', dt2.Column()],
            ['cupon(bd)', dt2.Column()],
            ['detalle(bd)', dt2.LinkColumn(A('model_change'), args=[A('detalle(bd)_id')], text=detalle_bd_nombre)],
            ['importe(bd)', dt2.Column()],
        ],
        '6col_f_y_bd': [
            ['cupon(bd)', dt2.Column()],
            ['detalle(bd)', dt2.LinkColumn(A('model_change'), args=[A('detalle(bd)_id')], text=detalle_bd_nombre)],
            ['importe(bd)', dt2.Column()],
            ['cupon(f)', dt2.Column()],
            ['detalle(f)', dt2.Column()],
            ['importe(f)', dt2.Column()],
        ],
        '6col_bd_y_f_sin_relacion': [
            ['cupon(f)', dt2.Column()],
            ['detalle(f)', dt2.Column()],
            ['importe(f)', dt2.Column()],
            ['cupon(bd)', dt2.Column()],
            ['detalle(bd)', dt2.LinkColumn(A('model_change'), args=[A('detalle(bd)_id')], text=detalle_bd_nombre)],
            ['importe(bd)', dt2.Column()],
        ]
    }

    # Para cada entrada de diccionario (nombre de la tabla comparativa) hay un diccionario de 3 elementos que
    # contiene el título, las columnas y los datos.
    datos_tablas = {}

    # Crear datos para cada tabla comparativa
    print('-----+ Crear datos para cada tabla comparativa +------')
    for tabla_comparativa in ctn.comparativas:
        print(ctn.comparativas[tabla_comparativa])

        # La variable tabla contiene el nombre de cada una
        # de las tablas comparativas, por ejemplo: gastos_posibles_sin_cupon
        # En datos_tabla está guardado el diccionario que tiene el título, el tipo y los datos de cada tabla
        datos_tabla = ctn.comparativas[tabla_comparativa]

        # Lista de elementos comparables: esta variable contiene los QuerySet o listas con los registros
        lista_elem_comp = datos_tabla['datos']

        # El tipo de tabla, con esto se sabe que columnas lleva y cómo distribuir los datos en la tabla
        tipo = datos_tabla['tipo']

        # Diccionario con los datos de la tabla
        nueva_tabla = {
            tabla_comparativa: {
                'columnas': tipos_tablas[tipo], 'titulo': datos_tabla['titulo'], 'datos': []
            }
        }

        # Depende de las columnas que lleve es cómo se llenan los datos en la tabla
        if tipo == '4col_f':
            for item in lista_elem_comp:
                nombre_entidad_financiera = 'Tarjeta Naranja'
                entidad_financiera = Entidad_financiera.objects.filter(nombre=nombre_entidad_financiera).first()
                entidad_financiera_str = "entidad_financiera=" + \
                                         str(entidad_financiera.pk) + '&' if entidad_financiera else ''
                titular = Titular.objects.filter(nombre='Nosotros').first()
                titular_str = "titular=" + str(titular.pk) + '&' if titular else ''
                concepto = Concepto.objects.filter(descripcion__icontains=item.detalle).first()
                concepto_str = "concepto=" + str(concepto.pk) + '&' if concepto else ''
                cantidad_cuotas = str(int(item.cuota)) if item.cuota and item.cuota.isdecimal() else '1'
                cupon_str = "cupon=" + item.cupon + "&"
                cantidad_cuotas_str = "cantidad_cuotas=" + cantidad_cuotas + "&"
                concepto_no_encontrado = "concepto_no_encontrado=" + item.detalle + "&" if not concepto else ''
                observacion_str = "observacion=&"
                mes_inicio_str = "mes_inicio=" + \
                                 Cierre.obtener_mes_actual(nombre_entidad_financiera).strftime('%Y-%m-%d') + "&"
                es_moneda_extranjera = True if item.moneda != 'P' else False
                es_moneda_extranjera_str = "es_moneda_extranjera=" + str(es_moneda_extranjera) + "&"
                datos = {
                    'entidad_financiera': entidad_financiera.pk if entidad_financiera else '',
                    'titular': titular.pk if titular else '',
                    'concepto': concepto.pk if concepto else '',
                    'cuotas': int(item.cuota) if item.cuota and item.cuota.isdecimal() else 1,
                    'es_moneda_extranjera': es_moneda_extranjera,
                    'importe': item.importe,
                    'parametros_url': (
                        titular_str + entidad_financiera_str + concepto_str + cupon_str +
                        cantidad_cuotas_str + observacion_str + concepto_no_encontrado +
                        mes_inicio_str + es_moneda_extranjera_str + "_popup=1"
                    )
                }
                # "importe={{ record|get:'importe(f)'|unlocalize }}&"
                nueva_tabla[tabla_comparativa]['datos'].append(
                    {
                        'cupon(f)': item.cupon, 'detalle(f)': item.detalle, 'importe(f)': item.importe,
                        'cuotas(f)': item.cuota, 'item_pk': item.pk, 'datos': datos
                    }
                )
        elif tipo == '2col_bd_y_f':
            nueva_tabla[tabla_comparativa]['datos'] = [
                {
                    'base de datos': lista_elem_comp['bd'],
                    'factura': lista_elem_comp['factura']
                }
            ]
        elif tipo == 'total_importes_diferencia':
            nueva_tabla[tabla_comparativa]['datos'] = [
                {
                    'base de datos': lista_elem_comp['bd'],
                    'factura': lista_elem_comp['factura'],
                    'diferencia': lista_elem_comp['diferencia']
                }
            ]
        elif tipo == '6col_bd_y_f':
            for item in lista_elem_comp:
                for item_bd in item["bd"]:
                    # Se traen los datos de diferentes modelos (tablas) dependiendo de si es un Gasto o un Permanente
                    model = model_change[item_bd._meta.object_name]
                    if item_bd._meta.object_name == 'Gasto':
                        detalle_bd = item_bd.movimiento.concepto.descripcion
                    elif item_bd._meta.object_name == 'Permanente':
                        detalle_bd = item_bd.concepto.descripcion
                    else:
                        detalle_bd = None
                    # detalle(bd)_id guarda el Primary Key del modelo donde está el gasto
                    # model_change guarda el URL del Admin de Django según sea Gasto o Permanente
                    nueva_tabla[tabla_comparativa]['datos'].append(
                        {
                            'cupon(f)': item["factura"].cupon, 'detalle(f)': item["factura"].detalle,
                            'importe(f)': item["factura"].importe,
                            'cupon(bd)': item_bd.cupon, 'detalle(bd)': detalle_bd,
                            'importe(bd)': item_bd.importe, 'detalle(bd)_id': item_bd.pk, 'model_change': model
                        }
                    )
        elif tipo == '6col_f_y_bd':
            for item in lista_elem_comp:
                for item_factura in item["factura"]:
                    model = model_change[item['bd']._meta.object_name]
                    if item['bd']._meta.object_name == 'Gasto':
                        detalle_bd = item['bd'].movimiento.concepto.descripcion
                    elif item['bd']._meta.object_name == 'Permanente':
                        detalle_bd = item['bd'].concepto.descripcion
                    else:
                        detalle_bd = None
                    nueva_tabla[tabla_comparativa]['datos'].append(
                        {
                            'cupon(bd)': item['bd'].cupon, 'detalle(bd)': detalle_bd,
                            'importe(bd)': item['bd'].importe, 'detalle(bd)_id': item['bd'].pk, 'model_change': model,
                            'cupon(f)': item_factura.cupon, 'detalle(f)': item_factura.detalle,
                            'importe(f)': item_factura.importe
                        }
                    )
        elif tipo == '6col_bd_y_f_sin_relacion':
            item_mix = None
            if lista_elem_comp:
                for item in lista_elem_comp:
                    item_mix = itertools.zip_longest(item['factura'], item['bd'])
                for item_factura, item_bd in item_mix:
                    cupon_bd = None
                    importe_bd = None
                    detalle_bd = None
                    detalle_bd_id = None
                    cupon_f = None
                    detalle_f = None
                    importe_f = None
                    model = None
                    # Se traen los datos de diferentes modelos (tablas) dependiendo de si es un Gasto o un Permanente
                    if item_bd:
                        model = model_change[item_bd._meta.object_name]
                        if item_bd._meta.object_name == 'Gasto':
                            detalle_bd = item_bd.movimiento.concepto.descripcion
                        elif item_bd._meta.object_name == 'Permanente':
                            detalle_bd = item_bd.concepto.descripcion
                        else:
                            detalle_bd = None
                        cupon_bd = item_bd.cupon
                        importe_bd = item_bd.importe
                        detalle_bd_id = item_bd.pk
                    if item_factura:
                        cupon_f = item_factura.cupon
                        detalle_f = item_factura.detalle
                        importe_f = item_factura.importe
                    nueva_tabla[tabla_comparativa]['datos'].append(
                        {
                            'cupon(f)': cupon_f, 'detalle(f)': detalle_f, 'importe(f)': importe_f,
                            'cupon(bd)': cupon_bd, 'detalle(bd)': detalle_bd, 'importe(bd)': importe_bd,
                            'detalle(bd)_id': detalle_bd_id, 'model_change': model
                        }
                    )
        else:
            pass

        datos_tablas.update(nueva_tabla)

    return datos_tablas


@login_required
def excluir_gasto_resumen(request, item_pk):
    item = ResumenTNTemp.objects.get(pk=item_pk)
    print(f'item: {item}')
    item.excluir()
    return redirect('procesar_datos_naranja')
