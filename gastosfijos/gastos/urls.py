# from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('proximos', views.proximos, name='proximos'),
    path('tabla', views.tabla, name='tabla'),
    path('tabla-test', views.tabla_test, name='tabla_test'),
    path('dict-test', views.dict_test, name='dict_test'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('procesos/extraer_resumen_naranja', views.extraer_resumen_naranja, name='extraer_resumen_naranja'),
    path('procesos/procesar_datos_naranja', views.procesar_datos_naranja, name='procesar_datos_naranja'),
    path('excluir_gasto_resumen/<int:item_pk>', views.excluir_gasto_resumen, name='excluir_gasto_resumen'),
]
