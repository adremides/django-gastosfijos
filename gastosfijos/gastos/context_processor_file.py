from gastosfijos import version
from .models import Cierre


def datos_generales(request):
    nombre_entidad_financiera = 'Tarjeta Naranja'
    mes_actual = Cierre.obtener_mes_actual(nombre_entidad_financiera)
    # print(mes_actual)
    return {
        "app_version": version.get(),
        "app_remote_version": version.get_remote(),
        "mes_actual": mes_actual,
    }
