# Generated by Django 3.2.5 on 2022-07-03 22:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gastos', '0003_auto_20220324_2208'),
    ]

    operations = [
        migrations.AddField(
            model_name='titular',
            name='entidades_financieras',
            field=models.ManyToManyField(blank=True, help_text='Entidades financieras a las que está adherido el usuario.', to='gastos.Entidad_financiera', verbose_name='entidades_financieras'),
        ),
    ]
