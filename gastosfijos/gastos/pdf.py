import re
import PyPDF2


class TarjetaNaranja(object):

    def __init__(self, archivo_pdf):
        self.log = []
        # Datos estáticos
        self.posiciones = self.posiciones_de_los_campos()

        # Leer PDF
        pdf_reader = PyPDF2.PdfFileReader(archivo_pdf)
        self. num_paginas = pdf_reader.numPages

        # Obtener páginas con información
        self.pagina_inicial = pdf_reader.getPage(0)
        self.pagina_detalle = pdf_reader.getPage(1)
        self.texto_detalle = self.pagina_detalle.extractText()
        self.texto_inicial = self.pagina_inicial.extractText()

        # Obtener datos de información inicial
        total_inicio = self.texto_inicial.find('Tu total a pagar es')
        total_fin = self.texto_inicial.find('y vence el')
        total_texto = self.texto_inicial[total_inicio+19:total_fin]
        if '+' in total_texto:
            self.total_importe = total_texto.split('+')[0].strip()[1:]
            self.total_dolares = total_texto.split('+')[1].strip()[3:]
        else:
            self.total_importe = total_texto[1:]
            self.total_dolares = 0

        vencimiento_inicio = total_fin+11
        vencimiento_fin = total_fin+19
        vencimiento_texto = self.texto_inicial[vencimiento_inicio:vencimiento_fin]
        self.fecha_vencimiento = vencimiento_texto

        # Establecer rango de ubicación de información detalle
        gastos_ma_inicio = self.texto_detalle.find('FECHA')
        gastos_ma_fin = self.texto_detalle.find('Consumos de Yamil')
        gastos_ma_texto = self.texto_detalle[gastos_ma_inicio:gastos_ma_fin]

        gastos_nosotros_inicio = self.texto_detalle.find('FECHA', gastos_ma_inicio + 5)
        gastos_nosotros_fin = self.texto_detalle.find("Otroscargos")
        gastos_nosotros_texto = self.texto_detalle[gastos_nosotros_inicio:gastos_nosotros_fin]

        mantenimiento_inicio = self.texto_detalle.find('*COMISION POR MANTENIMIENTO DE CUENTA')
        mantenimiento_fin = self.texto_detalle.find('Consumos de Yamil')
        mantenimiento_texto = self.texto_detalle[mantenimiento_inicio:mantenimiento_fin]
        self.importe_mantenimiento = mantenimiento_texto[-12:]

        iva_inicio = self.texto_detalle.find('IVA Operaciones Identificadas con *')
        iva_fin = self.texto_detalle.find('Impuesto de Sellos')
        iva_texto = self.texto_detalle[iva_inicio:iva_fin]
        self.importe_iva = iva_texto[-12:]

        sellos_inicio = self.texto_detalle.find('Impuesto de Sellos')
        sellos_fin = self.texto_detalle.find('Total$')
        sellos_texto = self.texto_detalle[sellos_inicio:sellos_fin]
        self.importe_sellos = sellos_texto[-12:]

        # print(gastos_ma_texto)
        # print(gastos_nosotros_texto)

        # Guardar texto de prueba
        self.texto_de_prueba = gastos_nosotros_texto

        # Generar variables con el texto obtenido
        gastos_ma = self.obtener_gastos(gastos_ma_texto)
        self.datos_ma = self.generar_datos(gastos_ma)
        # print("Gastos Ma:")
        # for elemento in self.datos_ma:
        #     print(elemento)

        gastos_nosotros = self.obtener_gastos(gastos_nosotros_texto)
        self.datos_nosotros = self.generar_datos(gastos_nosotros)
        # print("---\nGastos Nosotros:")
        # for elemento in self.datos_nosotros:
        #     print(elemento)

        # Fin de inicialización
        archivo_pdf.close()

    # noinspection PyMethodMayBeStatic
    def obtener_gastos(self, texto):
        r_fecha = r'\d{2}\/\d{2}\/\d{2}'  # Fecha con formato dd/mm/yy
        r_espacio = r'\s'                 # 1 espacio
        r_o = r'|'                        # esto es un "or", es lo anterior O lo que sigue
        r_cualquier_texto = r'.*?'        # 0 o más cualquier caracter lazy (la menor cantidad de caracteres)
        r_importe = r'-?[\d\.]?'          # 0 o 1 menos (-), 0 o 1 conjunto de carácteres dígitos y puntos
        r_importe += r'\d+,\d{2}'         # 1 o más dígitos, una coma y 2 dígitos
        r_importe += r'(?![^\(]*\))'      # texto que no esté encerrado entre paréntesis
        # r_importe puede ser en pesos o en dólares, cuando lleva uno, no lleva el otro
        gastos = re.findall(r_fecha + r_espacio + r'Naranja' + r_cualquier_texto + r_importe +
                            r_o +
                            r_fecha + r_espacio + r'Visa' + r_cualquier_texto + r_importe +
                            r_o +
                            r'Naranja' + r_cualquier_texto + r_importe +
                            r_o +
                            r'Visa' + r_cualquier_texto + r_importe,
                            texto)
        # codigo_regex = r_fecha + r_espacio + r'Naranja' + r_cualquier_texto + r_importe
        # codigo_regex += r_o
        # codigo_regex += r_fecha + r_espacio + r'Visa' + r_cualquier_texto + r_importe
        # codigo_regex += r_o
        # codigo_regex += r'Naranja' + r_cualquier_texto + r_importe
        # codigo_regex += r_o
        # codigo_regex += r'Visa' + r_cualquier_texto + r_importe
        # print(f'codigo regex: {codigo_regex}')
        return gastos

    # noinspection PyMethodMayBeStatic
    def generar_datos(self, lista_gastos):
        datos = []
        rangos_c = self.rango_de_los_campos()
        for elemento in lista_gastos:
            fila = {}
            tiene_fecha = not len(elemento) < 110
            if tiene_fecha:  # la línea es más corta porque no tiene fecha
                rangos = rangos_c['con_fecha']
                fila['fecha'] = elemento[rangos['fecha']]
            else:
                rangos = rangos_c['sin_fecha']
                fila['fecha'] = '--/--/--'
            fila['tarjeta'] = elemento[rangos['tarjeta']].strip()
            fila['cupon'] = elemento[rangos['cupon']].strip()
            fila['detalle'] = elemento[rangos['detalle']].strip()
            fila['cuota_plan'] = elemento[rangos['cuota_plan']].strip()
            fila['importe'] = elemento[rangos['importe']].strip()
            # if len(elemento) > 110:
            fila['dolares'] = elemento[rangos['dolares']].strip()
            # print('elemento: %s' % elemento)
            # print('fila: %s' % fila)
            datos.append(fila)
        return datos

    # noinspection PyMethodMayBeStatic
    def campos(self):
        return [
            'fecha',
            'tarjeta',
            'cupon',
            'detalle',
            'cuota_plan',
            'importe',
            'dolares'
        ]

    # noinspection PyMethodMayBeStatic
    def posiciones_de_los_campos(self):
        return {
            'con_fecha': {
                'fecha': 0,
                'tarjeta': 9,
                'cupon': 17,
                'detalle': 29,
                'cuota_plan': 88,
                'importe': 98,
                'dolares': 111
            },
            'sin_fecha': {
                'fecha': 0,  # No se usa, va porque sino da error en el bucle que genera los rangos
                'tarjeta': 0,
                'cupon': 8,
                'detalle': 20,
                'cuota_plan': 79,
                'importe': 89,
                'dolares': 111
            }
        }

    # noinspection PyMethodMayBeStatic
    def tamanio_de_los_campos(self):
        return {
                'fecha': 8,
                'tarjeta': 7,
                'cupon': 11,
                'detalle': 59,
                'cuota_plan': 9,
                'importe': 12,
                'dolares': 12
        }

    # noinspection PyMethodMayBeStatic
    def rango_de_los_campos(self):
        pos_cf = self.posiciones_de_los_campos()['con_fecha']
        pos_sf = self.posiciones_de_los_campos()['sin_fecha']
        tam = self.tamanio_de_los_campos()
        rangos = {'con_fecha': {}, 'sin_fecha': {}}
        for campo in self.campos():
            rangos['con_fecha'][campo] = slice(pos_cf[campo], pos_cf[campo] + tam[campo])
            rangos['sin_fecha'][campo] = slice(pos_sf[campo], pos_sf[campo] + tam[campo])
        return rangos
